USE [ReferralsTransfers]
GO
/****** Object:  Table [dbo].[Appointment]    Script Date: 12/18/2013 13:16:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Appointment](
	[Process_ID] [varchar](10) NOT NULL,
	[Logged] [datetime] NOT NULL,
	[User_Name] [nvarchar](50) NOT NULL,
	[User_Display] [nvarchar](75) NOT NULL,
	[Patient_Name] [nvarchar](75) NOT NULL,
	[Provider_Name] [nvarchar](75) NOT NULL,
	[Appt_Begin] [datetime] NOT NULL,
	[Appt_End] [datetime] NOT NULL,
	[Status] [nvarchar](50) NOT NULL,
	[Provider_Rep] [nvarchar](75) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Activity_Log]    Script Date: 12/18/2013 13:16:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Activity_Log](
	[Process_ID] [varchar](10) NOT NULL,
	[Entry_ID] [int] NOT NULL,
	[Logged] [datetime] NOT NULL,
	[Category_Type] [varchar](50) NULL,
	[User_Name] [varchar](50) NULL,
	[User_Display] [varchar](75) NULL,
	[Activity] [varchar](250) NOT NULL,
	[Entity_Name] [varchar](200) NULL,
	[Entity_Role] [varchar](50) NULL,
	[User_Notes] [text] NULL,
	[Category_Finished] [bit] NULL,
	[Link_Attachment_Path] [varchar](250) NULL,
	[Link_Attachment_Source] [varchar](250) NULL,
	[Process_State] [varchar](75) NULL,
	[Interaction_Type] [varchar](50) NULL,
	[Interaction_ID] [varchar](50) NULL,
	[Interaction_Origin] [varchar](50) NULL,
	[Interaction_Address] [varchar](75) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
