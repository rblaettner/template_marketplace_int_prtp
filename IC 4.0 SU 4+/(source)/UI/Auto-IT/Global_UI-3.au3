#RequireAdmin
#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_Icon=..\monitor.ico
#AutoIt3Wrapper_Outfile=Setup.exe
#AutoIt3Wrapper_Compression=4
#AutoIt3Wrapper_UseUpx=n
#AutoIt3Wrapper_Res_Comment=http://marketplace.inin.com
#AutoIt3Wrapper_Res_Description=Patient Referrals & Transfers
#AutoIt3Wrapper_Res_Fileversion=1.0.0.35
#AutoIt3Wrapper_Res_Fileversion_AutoIncrement=y
#AutoIt3Wrapper_Res_LegalCopyright=� 2013 Interactive Intelligence
#AutoIt3Wrapper_Res_Language=1033
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****

; *** Start added by AutoIt3Wrapper ***
#include <EditConstants.au3>
#include <WindowsConstants.au3>
; *** End added by AutoIt3Wrapper ***

#include <Math.au3>
#include <GuiRichEdit.au3>
#include <GUIConstantsEx.au3>
#include <GuiListView.au3>
#include <GuiEdit.au3>
#include <Security.au3>
#include <Misc.au3>

AutoItSetOption( "WinTitleMatchMode", 2 )

Global Const $PRODUCT_TITLE = "Patient Referrals Process for IC 4.0"
Global Const $PRODUCT_VERSION = "1.0"
Global Const $PRODUCT_DEVELOPER = "Interactive Intelligence, Inc."

Dim Const $PRODUCT_SUBTITLE = "Coordinated patient referrals for highly effective practitioners"
Dim Const $PRODUCT_URL_ABOUT = "http://marketplace.inin.com"
Dim Const $PRODUCT_URL_SUPPORT = ""
Dim Const $PRODUCT_URL_HELP = ""

Global Const $UI_INSTALL_NAME = "Setup Wizard"
Global Const $UI_WINDOW_TITLE = StringReplace( $PRODUCT_TITLE, "&&", "&" ) & " " & $UI_INSTALL_NAME
Global Const $UI_INSTALL_LANGUAGE = "en_US"

Global $dbOdbcDsn, $dbOdbcDbo

Global Const $FILE_PATH_INSTALL_TEMP = @TempDir & "\" & $PRODUCT_TITLE & "\" & $PRODUCT_VERSION & "\" & "Installer"

Dim Const $FILE_PATH_ABOUT_TEXT =  @TempDir & "\" & $PRODUCT_TITLE & "\" & $PRODUCT_VERSION & "\" & "About\Description.RTF"
Dim Const $FILE_PATH_LICENSE_TEXT = @TempDir & "\" & $PRODUCT_TITLE & "\" & $PRODUCT_VERSION & "\" & "License\EULA.RTF"

Dim Const $FILE_PATH_UI_BACKGROUNDS = $FILE_PATH_INSTALL_TEMP & "\UI\Backgrounds"
Dim Const $FILE_PATH_UI_BUTTONS = $FILE_PATH_INSTALL_TEMP & "\UI\Buttons"

#include "NT_Auth-2.AU3"
#include "Install.AU3"

_Singleton( "singleton", 0 )

DirCreate( $FILE_PATH_UI_BACKGROUNDS )
DirCreate( $FILE_PATH_UI_BUTTONS )

DirCreate( $FILE_PATH_ABOUT_TEXT )
DirCreate( $FILE_PATH_LICENSE_TEXT )

DirRemove( $FILE_PATH_ABOUT_TEXT )
DirRemove( $FILE_PATH_LICENSE_TEXT )

FileInstall( "..\Close.BMP", $FILE_PATH_UI_BUTTONS & "\", 1 )
FileInstall( "..\Next.BMP", $FILE_PATH_UI_BUTTONS & "\", 1 )

FileInstall( "..\Back.BMP", $FILE_PATH_UI_BUTTONS & "\", 1 )
FileInstall( "..\Setup.BMP", $FILE_PATH_UI_BUTTONS & "\", 1 )
FileInstall( "..\Install.BMP", $FILE_PATH_UI_BUTTONS & "\", 1 )

FileInstall( ".\0.BMP", $FILE_PATH_UI_BACKGROUNDS & "\", 1 )
FileInstall( ".\3.BMP", $FILE_PATH_UI_BACKGROUNDS & "\", 1 )
FileInstall( ".\5.BMP", $FILE_PATH_UI_BACKGROUNDS & "\", 1 )

FileInstall( ".\6.BMP", $FILE_PATH_UI_BACKGROUNDS & "\", 1 )
FileInstall( ".\7.BMP", $FILE_PATH_UI_BACKGROUNDS & "\", 1 )
FileInstall( ".\8.BMP", $FILE_PATH_UI_BACKGROUNDS & "\", 1 )
FileInstall( ".\9.BMP", $FILE_PATH_UI_BACKGROUNDS & "\", 1 )

FileInstall( ".\MarketPlace.BMP", $FILE_PATH_UI_BACKGROUNDS & "\", 1 )

FileInstall( ".\EULA.RTF", $FILE_PATH_LICENSE_TEXT, 1 )
FileInstall( ".\Description.RTF", $FILE_PATH_ABOUT_TEXT, 1 )


#include "test.au3"



;
;
;
;


; Text Formatting...

Local $fontStyle, $fontSize, $fontWeight, $fontAttributes, $fontName, $fontColor

Dim Const $UI_FONT_STYLE = 0
Dim Const $UI_FONT_SIZE = 1
Dim Const $UI_FONT_WEIGHT = 2
Dim Const $UI_FONT_ATTRIBUTES = 3
Dim Const $UI_FONT_NAME = 4
Dim Const $UI_FONT_COLOR_FG = 5
Dim Const $UI_FONT_COLOR_BG = 6

   ; Tab Title Text:
	$fontStyle = "Title"
    $fontSize = 12
    $fontWeight = 400
    $fontAttributes = 0
    $fontName = "Segoe UI"
    $fontColorFG = 0xFFFFFF
	$fontColorBG = $GUI_BKCOLOR_TRANSPARENT

	Dim Const $UI_FONT_TITLE[7] = [ $fontStyle, $fontSize, $fontWeight, $fontAttributes, $fontName, $fontColorFG, $fontColorBG ]


	; Default Text:
	$fontStyle = "Default"
    $fontSize = 12
    $fontWeight = 400
    $fontAttributes = 0
    $fontName = "Segoe UI"
    $fontColorFG = 0x000000
	$fontColorBG = $GUI_BKCOLOR_TRANSPARENT

	Dim Const $UI_FONT_DEFAULT[7] = [ $fontStyle, $fontSize, $fontWeight, $fontAttributes, $fontName, $fontColorFG, $fontColorBG ]


	; Bold Text:
	$fontStyle = "Heavy"
	; $fontSize = 12
    $fontWeight = 800
	; $fontAttributes = 0
	; $fontName = "Segoe UI"
    $fontColorFG = 0x000000
	; $fontColorBG = $GUI_BKCOLOR_TRANSPARENT

	Dim Const $UI_FONT_HEAVY[7] = [ $fontStyle, $fontSize, $fontWeight, $fontAttributes, $fontName, $fontColorFG, $fontColorBG ]


	; Light Text:
	$fontStyle = "Light"
	; $fontSize = 12
    $fontWeight = 400
	; $fontAttributes = 0
	; $fontName = "Segoe UI"
    $fontColorFG = 0xA8A8A8
	; $fontColorBG = $GUI_BKCOLOR_TRANSPARENT

	Dim Const $UI_FONT_LIGHT[7] = [ $fontStyle, $fontSize, $fontWeight, $fontAttributes, $fontName, $fontColorFG, $fontColorBG ]


	; Link Text:
	$fontStyle = "Link"
	; $fontSize = 12
    ; $fontWeight = 400
	$fontAttributes = 4
	; $fontName = "Segoe UI"
    $fontColorFG = 0x0000FF
	; $fontColorBG = $GUI_BKCOLOR_TRANSPARENT

	Dim Const $UI_FONT_LINK[7] = [ $fontStyle, $fontSize, $fontWeight, $fontAttributes, $fontName, $fontColorFG, $fontColorBG ]

	; Semi-Bold Text:
	$fontStyle = "Semi"
	; $fontSize = 12
    ; $fontWeight = 400
	$fontAttributes = 0
	$fontName = "Segoe UI Semibold"
    $fontColorFG = 0x000000
	; $fontColorBG = $GUI_BKCOLOR_TRANSPARENT

	Dim Const $UI_FONT_SEMI[7] = [ $fontStyle, $fontSize, $fontWeight, $fontAttributes, $fontName, $fontColorFG, $fontColorBG ]

;
;
;
;

; Tab Data...

Dim Const $UI_TAB_MAIN = $TabMain

Local $tabPage, $txtTitle, $imgCtrl, $imgPath, $ctrlHeader, $txtHeader, $ctrlFooter, $txtFooter, $imgLeft, $imgRight

Dim Const $UI_TAB_PAGE_CTRL = 1
Dim Const $UI_TAB_TITLE_TEXT = 2
Dim Const $UI_TAB_IMG_CTRL = 3
Dim Const $UI_TAB_IMG_PATH = 4
Dim Const $UI_TAB_HEADER_CTRL = 5
Dim Const $UI_TAB_HEADER_TEXT = 6
Dim Const $UI_TAB_FOOTER_CTRL = 7
Dim Const $UI_TAB_FOOTER_TEXT = 8
Dim Const $UI_TAB_NAV_IMG_LEFT = 9
Dim Const $UI_TAB_NAV_IMG_RIGHT = 10

   ;
   ;

   ; Tab: Initialize

   $tabPage = $TabWelcome
   $txtTitle = "Preparing " & $UI_INSTALL_NAME
   $imgCtrl = $WelcomePic
   $imgPath = "3"

   $ctrlHeader = $WelcomeHeader
   $txtHeader = "Please wait while the " & $UI_INSTALL_NAME & " prepares to guide you through the " & $PRODUCT_TITLE & " installation."

   $ctrlFooter = 0
   $txtFooter = ""

   $imgLeft = ""
   $imgRight = ""

   Dim Const $UI_TAB_INITIALIZE_DATA[11] = [ 10, $tabPage, $txtTitle, $imgCtrl, $imgPath, $ctrlHeader, $txtHeader, $ctrlFooter, $txtFooter, $imgLeft, $imgRight ]

   ; Tab: Welcome

   $tabPage = $TabWelcome
   $txtTitle = "Welcome to the " & $UI_INSTALL_NAME
   $imgCtrl = $WelcomePic
   $imgPath = "3"

   $ctrlHeader = $WelcomeHeader
   $txtHeader = "The " & $UI_INSTALL_NAME & " will install " & $PRODUCT_TITLE & " for Interaction Center. " & _
   "Click Next to continue or close the window to exit the " & $UI_INSTALL_NAME & "."

   $ctrlFooter = ""
   $txtFooter = ""

   $imgLeft = "Close"
   $imgRight = "Next"

   Dim Const $UI_TAB_WELCOME_DATA[11] = [ 10, $tabPage, $txtTitle, $imgCtrl, $imgPath, $ctrlHeader, $txtHeader, $ctrlFooter, $txtFooter, $imgLeft, $imgRight ]


   ; Tab: About

   $tabPage = $TabAbout
   $txtTitle = $PRODUCT_TITLE
   $imgCtrl = $AboutPic
   $imgPath = "5"

   $ctrlHeader = $AboutHeader
   $txtHeader = $PRODUCT_SUBTITLE

   $ctrlFooter = $AboutFooter
   $txtFooter = "Learn more about " & $PRODUCT_TITLE & " at MarketPlace(SM)"

   $imgLeft = "Back"
   $imgRight = "Setup"

   Dim Const $UI_TAB_ABOUT_DATA[11] = [ 10, $tabPage, $txtTitle, $imgCtrl, $imgPath, $ctrlHeader, $txtHeader, $ctrlFooter, $txtFooter, $imgLeft, $imgRight ]

	; Tab: License

   $tabPage = $TabLicense
   $txtTitle = "License Agreement"
   $imgCtrl = $LicensePic
   $imgPath = "5"

   $ctrlHeader = $LicenseHeader
   $txtHeader = "To continue you must read and accept all terms of this agreement. If you do not want to accept the " & $PRODUCT_DEVELOPER & " License Terms, close this window to cancel the installation."

	GUICtrlSetData( $chkRequirement01, " " & "Process Automation licensing  (server, designer, template)" )
	GUICtrlSetData( $chkRequirement02, " " & "Documentation for database and web server configuration" )
	GUICtrlSetData( $chkRequirement03, " " & "Administrative access to IC, Microsoft web and database servers" )
	GUICtrlSetData( $chkRequirement04, " " & "Training or experience with Interaction Process Automation" )
	GUICtrlSetData( $chkRequirement05, " " & "This installer assists with processes, handlers, and DS entries." )

   $ctrlFooter = $LicenseCheckbox
   $txtFooter = ( " " & "My organization accepts the terms of this agreement." )

   $imgLeft = "Back"
   $imgRight = "Next"

   Dim Const $UI_TAB_LICENSE_DATA[11] = [ 10, $tabPage, $txtTitle, $imgCtrl, $imgPath, $ctrlHeader, $txtHeader, $ctrlFooter, $txtFooter, $imgLeft, $imgRight ]

	; Tab: Requirements

   $tabPage = $TabRequirements
   $txtTitle = "Requirements"
   $imgCtrl = $RequirementsPic
   $imgPath = "5"

   $ctrlHeader = $RequirementsHeader
   $txtHeader = "Please verify each of the following requirements for " & $PRODUCT_TITLE & " before continuing with installation."

   $ctrlFooter = $RequirementsFooter
   $txtFooter = $UI_INSTALL_NAME & " will gather information to install " & $PRODUCT_TITLE & ". The installation process will take approximately 5 minutes to complete."

   $imgLeft = "Back"
   $imgRight = "Next"

   Dim Const $UI_TAB_REQUIREMENTS_DATA[11] = [ 10, $tabPage, $txtTitle, $imgCtrl, $imgPath, $ctrlHeader, $txtHeader, $ctrlFooter, $txtFooter, $imgLeft, $imgRight ]

   ; Tab: Environment

   $tabPage = $TabEnvironment
   $txtTitle = "Setup Environment"
   $imgCtrl = $EnvironmentPic
   $imgPath = "5"

   $ctrlHeader = $EnvironmentHeader
   $txtHeader = "To coordinate setup activities, " & $PRODUCT_TITLE & " " & $UI_INSTALL_NAME & " needs to know about your target operating environment and server infrastructure."

   $ctrlFooter = 0
   $txtFooter = ""

   $imgLeft = "Back"
   $imgRight = "Next"

   Dim Const $UI_TAB_ENVIRONMENT_DATA[11] = [ 10, $tabPage, $txtTitle, $imgCtrl, $imgPath, $ctrlHeader, $txtHeader, $ctrlFooter, $txtFooter, $imgLeft, $imgRight ]

	; Tab: Credentials Master

   $tabPage = $TabCredentials
   $txtTitle = "Resource Authorization"
   $imgCtrl = $CredentialsPic
   $imgPath = "5"

   $ctrlHeader = $CredentialsHeader
   $txtHeader = "%Resource% requires %Role% authorization for configuration purposes. Please provide the appropriate account credentials below. To verify information, click Authenticate."

   $ctrlFooter = $CredentialsFooter
   $txtFooter = ""

   $imgLeft = "Back"
   $imgRight = "Next"

   Dim Const $UI_TAB_CREDENTIALS_DATA[11] = [ 10, $tabPage, $txtTitle, $imgCtrl, $imgPath, $ctrlHeader, $txtHeader, $ctrlFooter, $txtFooter, $imgLeft, $imgRight ]

   ; ===========

   $imgPath = "6"
   Dim Const $UI_TAB_CREDENTIALS_01_DATA[11] = [ 10, $tabPage, $txtTitle, $imgCtrl, $imgPath, $ctrlHeader, $txtHeader, $ctrlFooter, $txtFooter, $imgLeft, $imgRight ]

	$imgLeft = "Back"
   $imgRight = "Install"

   $imgPath = "7"
   Dim Const $UI_TAB_CREDENTIALS_02_DATA[11] = [ 10, $tabPage, $txtTitle, $imgCtrl, $imgPath, $ctrlHeader, $txtHeader, $ctrlFooter, $txtFooter, $imgLeft, $imgRight ]

   $imgPath = "9"
   Dim Const $UI_TAB_CREDENTIALS_03_DATA[11] = [ 10, $tabPage, $txtTitle, $imgCtrl, $imgPath, $ctrlHeader, $txtHeader, $ctrlFooter, $txtFooter, $imgLeft, $imgRight ]

   $imgPath = "8"
   Dim Const $UI_TAB_CREDENTIALS_04_DATA[11] = [ 10, $tabPage, $txtTitle, $imgCtrl, $imgPath, $ctrlHeader, $txtHeader, $ctrlFooter, $txtFooter, $imgLeft, $imgRight ]

   $imgPath = "0"
   Dim Const $UI_TAB_CREDENTIALS_05_DATA[11] = [ 10, $tabPage, $txtTitle, $imgCtrl, $imgPath, $ctrlHeader, $txtHeader, $ctrlFooter, $txtFooter, $imgLeft, $imgRight ]

Dim Const $UI_TAB_MAIN_PAGES[12] = [ 11, $UI_TAB_INITIALIZE_DATA, $UI_TAB_WELCOME_DATA, $UI_TAB_ABOUT_DATA, $UI_TAB_LICENSE_DATA, $UI_TAB_REQUIREMENTS_DATA, $UI_TAB_ENVIRONMENT_DATA, $UI_TAB_CREDENTIALS_01_DATA, $UI_TAB_CREDENTIALS_02_DATA, $UI_TAB_CREDENTIALS_03_DATA, $UI_TAB_CREDENTIALS_04_DATA, $UI_TAB_CREDENTIALS_05_DATA ]

Dim $tabMainPageIndex = GUICtrlRead( $TabMain ) + 1

; Control adjustments
Dim $RichText = ( _GUICtrlRichEdit_Create( $Window, "", 1, 1, 1, 1, BitOR( $ES_READONLY, $ES_MULTILINE, $WS_VSCROLL ) ) )
WinMove( $RichText, "", 0, 0, 0, 0 )

; Replace AboutText and LicenseText edit boxes with rich edit boxes...

GUICtrlSetState( $AboutText, $GUI_HIDE )
GUICtrlSetState( $LicenseText, $GUI_HIDE )

WinSetTitle( $Window, "", $UI_WINDOW_TITLE )

TraySetState( 2 )

ui_ShowTab( 2 )
GUISetState( @SW_SHOW, $Window )

;
;
;
;

Func ui_SetImage( $control, $imageName )

   GUICtrlSetImage( $control, $FILE_PATH_UI_BUTTONS & "\" & $imageName & ".BMP" )

EndFunc

Func ui_SetFontStyle( $control, $fontStyle )

	GUICtrlSetFont( $control, $fontStyle[$UI_FONT_SIZE], $fontStyle[$UI_FONT_WEIGHT], $fontStyle[$UI_FONT_ATTRIBUTES], $fontStyle[$UI_FONT_NAME] )
	GUICtrlSetColor( $control, $fontStyle[$UI_FONT_COLOR_FG] )
	GUICtrlSetBkColor( $control, $fontStyle[$UI_FONT_COLOR_BG] )

EndFunc

Func ui_ControlCenterPos( $control, $left, $width )

	Return ( $left + ( $width / 2 ) )

EndFunc

Func ui_ControlMiddlePos( $control, $top, $height )

	Return ( $top + ( $height / 2 ) )

EndFunc


Func ui_MatchBackground( $control, $x, $y )

	Local $color

	GUICtrlSetState( $control, $GUI_HIDE )

	Sleep( 100 )

	$color = PixelGetColor( $x, $y, $Window )
	GUICtrlSetBkColor( $control, $color )

	GUICtrlSetState( $control, $GUI_SHOW )

EndFunc




Func ui_ShowTab( $tabIndex )

   If ( $tabMainPageIndex == $tabIndex ) Then Return

   If ( $tabIndex < 1 ) Then

	  $tabIndex = 1

   ElseIf ( $tabIndex > $UI_TAB_MAIN_PAGES[0] ) Then

	  $tabIndex = $UI_TAB_MAIN_PAGES[0]

   EndIf

   $dataHide = $UI_TAB_MAIN_PAGES[ $tabMainPageIndex ]
   $dataShow = $UI_TAB_MAIN_PAGES[ $tabIndex ]

   $ctrlHide = $dataHide[ $UI_TAB_PAGE_CTRL ]
   $ctrlShow = $dataShow[ $UI_TAB_PAGE_CTRL ]

	GUICtrlSetState( $NavLeft, $GUI_DISABLE )
	GUICtrlSetState( $NavRight, $GUI_DISABLE)

   If ( Not ui_BeforeHide( $ctrlHide, $ctrlShow ) ) Then Return

   	  GUISetState( $GUI_DISABLE )
	  GUICtrlSetState( $ctrlHide, $GUI_HIDE )

   If ( Not ui_AfterHide( $ctrlHide ) ) Then Return

   $tabMainPageIndex = $tabIndex

    GUICtrlSetData( $MainTitle, $dataShow[ $UI_TAB_TITLE_TEXT ] )

   If ( ( $dataShow[ $UI_TAB_IMG_CTRL ] )  And ( $dataShow[ $UI_TAB_IMG_PATH ]  ) ) Then

	GUICtrlSetImage( $dataShow[ $UI_TAB_IMG_CTRL ], $FILE_PATH_UI_BACKGROUNDS & "\" & $dataShow[ $UI_TAB_IMG_PATH ] & ".BMP" )

   EndIf

        If( $dataShow[ $UI_TAB_HEADER_CTRL ] ) Then

   GUICtrlSetData( $dataShow[ $UI_TAB_HEADER_CTRL ], $dataShow[ $UI_TAB_HEADER_TEXT ] )

	EndIf

   If( $dataShow[ $UI_TAB_FOOTER_CTRL ] ) Then

	GUICtrlSetData( $dataShow[ $UI_TAB_FOOTER_CTRL ], $dataShow[ $UI_TAB_FOOTER_TEXT ] )

	EndIf

	ui_SetFontStyle( $dataShow[ $UI_TAB_HEADER_CTRL ], $UI_FONT_DEFAULT )
   ui_SetFontStyle( $dataShow[ $UI_TAB_FOOTER_CTRL ], $UI_FONT_DEFAULT )

	ui_SetImage( $NavLeft, $dataShow[ $UI_TAB_NAV_IMG_LEFT ] )
	ui_SetImage( $NavRight, $dataShow[ $UI_TAB_NAV_IMG_RIGHT ] )

GUICtrlSetState( $NavLeft, $GUI_ENABLE )
GUICtrlSetState( $NavRight, $GUI_ENABLE )

   If ( Not ui_BeforeShow( $ctrlShow ) ) Then Return
	  GUICtrlSetState( $ctrlShow, $GUI_SHOW )




If ( Not ui_AfterShow( $ctrlShow ) ) Then Return

GUISetState( $GUI_ENABLE )


EndFunc


Func ui_BeforeShow( $control )

   Switch $control

	  Case $TabWelcome

		 WinMove( $RichText, "", 0, 0, 0, 0 )

	  Case $TabAbout

		 _GUICtrlRichEdit_StreamFromFile( $RichText, $FILE_PATH_ABOUT_TEXT )
		 _GUICtrlRichEdit_SetScrollPos( $RichText, 0, 0 )

		 WinMove( $RichText, "", 32, 127, 573, 317 )

		 GUICtrlSetState( $NavRight, $GUI_ENABLE )
		 GUICtrlSetState( $NavRight, $GUI_DEFBUTTON )

		 ui_SetFontStyle( $AboutFooter, $UI_FONT_LINK )

	  Case $TabLicense

		 _GUICtrlRichEdit_StreamFromFile( $RichText, $FILE_PATH_LICENSE_TEXT )
		 _GUICtrlRichEdit_SetScrollPos( $RichText, 0, 0 )

		 WinMove( $RichText, "", 37, 166, 573, 265 )

		 If( BitAND( GUICtrlRead( $LicenseCheckbox ), $GUI_UNCHECKED ) ) Then GUICtrlSetState( $NavRight, $GUI_DISABLE )

	  Case $TabRequirements

		 WinMove( $RichText, "", 0, 0, 0, 0 )
		 RequirementCheckboxClick()

		Local $chkReqs[6] = [ 5, $chkRequirement01, $chkRequirement02, $chkRequirement03, $chkRequirement04, $chkRequirement05 ]

		For $i = 1 To $chkReqs[0]

			ui_SetFontStyle( $chkReqs[$i], $UI_FONT_SEMI )

		Next

		Case $TabEnvironment

		Local $rdosEnvironment[8] = [ 7, $rdoEnvDevelopment, $rdoEnvTestingQA, $rdoEnvTraining, $rdoEnvDemonstration, $rdoEnvProduction, $rdoEnvStage, $rdoEnvOther ]
		Local $lblsInfrastructure[4] = [ 3, $lblServerIC, $lblServerWeb, $lblServerDB ]
		Local $xOrigin = ui_ControlCenterPos( $rdosEnvironment[1], 30, 163 ), $yOrigin = ui_ControlMiddlePos( $rdosEnvironment[1], 41, 22 )
		Local $i = 0, $xOffset = 0, $yOffset = 29

		For $i = 1 To $rdosEnvironment[0]


			ui_MatchBackground( $rdosEnvironment[$i], ( $xOrigin + ( $xOffset * ( $i - 1 ) ) ), ( $yOrigin + ( $yOffset * ( $i - 1 ) ) ) )
			ui_SetFontStyle( $rdosEnvironment[$i], $UI_FONT_DEFAULT )


		Next

		ui_SetFontStyles( $lblsInfrastructure, $UI_FONT_DEFAULT )
		ui_SetFontStyle( $lblServerWebLink, $UI_FONT_LINK )

		ui_SetFontStyle( $grpEnvironments, $UI_FONT_SEMI )
		ui_SetFontStyle( $grpServers, $UI_FONT_SEMI )


		ui_MatchBackground( $grpEnvironments, ( ui_ControlCenterPos( $grpEnvironments, 32, 221 ) / 1.25 ), ( 113 - 1 ) )
		ui_MatchBackground( $grpServers, ( ui_ControlCenterPos( $grpServers, 284, 315 ) / 1.25 ), ( 113 - 1 ) )

		If ( GUICtrlRead( $txtServerIC ) == "" ) Then
			GUICtrlSetData( $txtServerIC, $IC_HOSTNAME )
		EndIf

		If ( GUICtrlRead( $txtServerWeb ) == "" ) Then
			GUICtrlSetData( $txtServerWeb, $dbOdbcDsn )
		EndIf

		If ( GUICtrlRead( $txtServerDB ) == "" ) Then
			GUICtrlSetData( $txtServerDB, $dbOdbcDbo )
		EndIf


	  Case $TabCredentials


		Local $roleNames[6] = [ 5, "Local Administrator", "IC Administrator", "", "", "" ]
		Local $resources[4] = [ 3, @ComputerName, GUICtrlRead( $txtServerIC ), GUICtrlRead( $txtServerWeb ) ]

		; Local $roleNames[6] = [ 5, "Product Installer", "NT Adminstrator", "IC Administrator", "Web Administrator", "DB Administrator" ]
		Local $roleCtrls[6] = [ 5, $lblCredsResource01, $lblCredsResource02, $lblCredsResource03, $lblCredsResource04, $lblCredsResource05 ]
		; Local $resources[6] = [ 5, @ComputerName, GUICtrlRead( $txtServerIC ), GUICtrlRead( $txtServerIC ), GUICtrlRead( $txtServerWeb ), GUICtrlRead( $txtServerDB ) ]
		Local $headerText = $UI_TAB_CREDENTIALS_DATA[ $UI_TAB_HEADER_TEXT ]

		Local $i = 0
		Local $subIndex = ( $tabMainPageIndex - 6 )
		Local $lblFooter = $UI_TAB_CREDENTIALS_DATA[ $UI_TAB_FOOTER_CTRL ]
		Local $lblHeader = $UI_TAB_CREDENTIALS_DATA[ $UI_TAB_HEADER_CTRL ]

		Local $credRecall
		$credRecall = nt_recall( $resources[ $subIndex ], $roleNames[ $subIndex] )

		GUICtrlSetData( $txtCredsDomain, $credRecall[ $NT_AUTH_DOMAIN_IDX ] )
		GUICtrlSetData( $txtCredsAccount, $credRecall[ $NT_AUTH_USER_IDX ] )
		GUICtrlSetData( $txtCredsPassword, $credRecall[ $NT_AUTH_PASSWORD_IDX ] )

		Switch $credRecall[ $NT_AUTH_STATUS_IDX ]

			Case $NT_AUTH_STATUS_PASS

				GUICtrlSetData ( $lblFooter, _
				$credRecall[ $NT_AUTH_USER_IDX ] & " previously authenticated." ) ;  & nt_dateFormat( $cred[ $NT_AUTH_RECORDED_IDX ] ) )

			Case $NT_AUTH_STATUS_FAIL

				GUICtrlSetData( $lblFooter, "Could not authenticate " & _
					$credRecall[ $NT_AUTH_USER_IDX ] & "..." ); & " on " & nt_dateFormat( $credRecall[ $NT_AUTH_RECORDED_IDX ] ) )

			Case Else

				; GUICtrlSetData( $lblFooter, "" )

		EndSwitch

		 $headerText = StringReplace( $headerText, "%Resource%", $resources[ $subIndex ] )
		 $headerText = StringReplace( $headerText, "%Role%", $roleNames[ $subIndex ] )

		 GUICtrlSetData( $lblHeader, $headerText )

		 If ( $subIndex == 1 ) Then

			GUICtrlSetData( $lblCredsResource01, $roleNames[1] )
			GUICtrlSetData( $lblCredsResource02, $roleNames[2] )
			GUICtrlSetData( $lblCredsResource03, $roleNames[3] )
			GUICtrlSetData( $lblCredsResource04, $roleNames[4] )
			GUICtrlSetData( $lblCredsResource05, $roleNames[5] )

			If ( $credRecall[ $NT_AUTH_STATUS_IDX ] == $NT_AUTH_STATUS_NONE ) Then

				GUICtrlSetData( $txtCredsDomain, @LogonDomain )
				GUICtrlSetData( $txtCredsAccount, @UserName )

			EndIf

		EndIf

		If ( $subIndex == 2 ) Then
			GUICtrlSetState( $btnCredsAuthenticate, $GUI_HIDE )

			GUICtrlSetState( $lblCredsDomain, $GUI_HIDE )
			GUICtrlSetState( $txtCredsDomain, $GUI_HIDE )
						GUICtrlSetState( $lblFooter, $GUI_HIDE )
		Else
			GUICtrlSetState( $btnCredsAuthenticate, $GUI_SHOW )

			GUICtrlSetState( $lblCredsDomain, $GUI_SHOW )
			GUICtrlSetState( $txtCredsDomain, $GUI_SHOW )
			GUICtrlSetState( $lblFooter, $GUI_SHOW )

		 EndIf

		 For $i = 1 To 5

			 If ( $i == $subIndex ) Then

				 ui_SetFontStyle( $roleCtrls[$i], $UI_FONT_HEAVY )

			 Else

				 ui_SetFontStyle( $roleCtrls[$i], $UI_FONT_LIGHT )

			EndIf

		 Next

ui_Refresh( $TabCredentials )


	  Case Else

		 Return True

   EndSwitch

   Return True

EndFunc


Func ui_AfterHide( $control )

   Switch $control

		Case $TabWelcome


		Case $TabCredentials

   EndSwitch

   Return True

EndFunc


Func ui_BeforeHide( $currentControl, $requestedControl )

	Switch $currentControl

		Case $TabEnvironment

			Local $msgResponse = 0, $i = 0
			Local $serverTypes[2] = [ 1, "Interaction Center (IC) Server" ] ; [ 3, "Interaction Center (IC) Server", "Microsoft IIS Web Server", "Microsoft SQL Database Server" ]
			Local $serverNames[2] = [ 1, $txtServerIC ] ; [ 3, $txtServerIC, $txtServerWeb, $txtServerDB ]

			GUISetState( @SW_DISABLE, $Window )

			If ( $requestedControl == $TabCredentials ) Then

				For $i = 1 To $serverTypes[0]

					GUICtrlSetData( $serverNames[$i], StringStripWS( GUICtrlRead( $serverNames[$i] ), 3 ) )

					If ( Ping( GUICtrlRead( $serverNames[$i] ), 3000 ) == 0 ) Then

						$msgResponse = MsgBox( 4 + 48, $PRODUCT_TITLE, ( "The designated " & $serverTypes[$i] & ", " & Chr(34) & GUICtrlRead( $serverNames[$i] ) & "," & Chr(34) & " did not respond. Continue anyway?" ), 0, $Window )

					EndIf

					If ( $msgResponse == 7 ) Then

						GUISetState( @SW_ENABLE, $Window )
						WinActivate( $Window )
						ControlFocus( $Window, "", $serverNames[$i] )
						_GUICtrlEdit_SetSel( $serverNames[$i], 0, -1 )

						Return False

					EndIf

				Next

		EndIf

		GUISetState( @SW_ENABLE, $Window )
		WinActivate ( $Window )

		Case $TabCredentials

			Local $domain = StringStripWS( GUICtrlRead( $txtCredsDomain ), 3 )
			Local $account = StringStripWS( GUICtrlRead( $txtCredsAccount ), 3 )
			Local $password = GUICtrlRead( $txtCredsPassword )

			Local $roles[4] = [ 3, "Local Administrator", "IC Administrator", "DB Administrator" ]
			Local $resources[4] = [ 3, GUICtrlRead( $txtServerIC ), GUICtrlRead( $txtServerIC ), GUICtrlRead( $txtServerWeb ) ]
			Local $txtCreds[4] = [ 3, $txtCredsDomain, $txtCredsAccount, $txtCredsPassword ]

			Local $subIndex = ( $tabMainPageIndex - 6 )

		nt_auth( $domain, $account, $password, $resources[ $subIndex ], $roles [$subIndex], True )


		Case Else

			Return True

	EndSwitch

   Return True

EndFunc


Func ui_AfterShow( $control )

	Switch $control

				Case $TabWelcome

					GUICtrlSetState( $NavRight, $GUI_DEFBUTTON )
						GUICtrlSetState( $NavRight, $GUI_FOCUS )

			Case $TabLicense

				ui_MatchBackground( $LicenseCheckbox, _
					ui_ControlCenterPos( $LicenseCheckbox, 32, 573 ), _
					ui_ControlMiddlePos( $LicenseCheckbox, 391, 28 ) _
				)

				ui_SetFontStyle( $LicenseCheckbox, $UI_FONT_DEFAULT )

					WinActivate( $RichText )
					LicenseCheckboxClick()

			Case $TabAbout

				GUICtrlSetState( $NavRight, $GUI_DEFBUTTON )
					WinActivate( $RichText )

Case $TabRequirements

	Local $chkReqs[6] = [ 5, $chkRequirement01, $chkRequirement02, $chkRequirement03, $chkRequirement04, $chkRequirement05 ]
	Local $chkTops[6] = [ 5, 138, 176, 214, 252, 290 ]

#comments-start
	For $i = 1 to $chkReqs[0]

		ui_MatchBackground( $chkReqs[$i], _
			ui_ControlCenterPos( $chkReqs[$i], 71, 501 ), _
			ui_ControlMiddlePos( $chkReqs[$i], $chkTops[$i], 28 ) _
		)


	Next
	#comments-end


		Case $TabCredentials

			Local $lblFooter = $UI_TAB_CREDENTIALS_DATA[ $UI_TAB_FOOTER_CTRL ]
			Local $subIndex = ( $tabMainPageIndex - 6 )
		Local $txtCreds[4] = [ 3, $txtCredsDomain, $txtCredsAccount, $txtCredsPassword ]

ui_Refresh( $TabCredentials )

GUISetState( $GUI_DISABLE )

ui_ValidateCreds()
Local Const $NT_DOMAIN_USER = ( GUICtrlRead( $txtCredsDomain ) & "\" & GUICtrlRead( $txtCredsAccount ) )
Local Const $SID_DOMAIN_TYPE = ( _Security__LookupAccountName( $NT_DOMAIN_USER ) )

		If ( ( GUICtrlRead( $lblFooter ) == "" ) And ( BitAND( GUICtrlGetState( $btnCredsAuthenticate ), $GUI_ENABLE ) ) ) Then

			GUICtrlSetData( $lblFooter, "Verifying " & _
				GUICtrlRead( $txtCredsAccount ) & " on " & _
				GUICtrlRead( $txtCredsDomain ) & "..." _
			)

		EndIf





				If ( GUICtrlRead( $txtCredsDomain ) == "" ) Then

					; GUICtrlSetData( $lblFooter, "" )
					GUICtrlSetState( $txtCredsDomain, $GUI_FOCUS )

				ElseIf ( GUICtrlRead( $txtCredsAccount ) == "" ) Then

						; GUICtrlSetData( $lblFooter, "" )
					GUICtrlSetState( $txtCredsAccount, $GUI_FOCUS )

						ElseIf ( _Security__GetAccountSid( $NT_DOMAIN_USER ) == 0 ) Then






					GUICtrlSetData( $lblFooter, StringReplace( GUICtrlRead( $lblFooter ), "...", ":" ) & @CRLF & "Account not found on " & GUICtrlRead( $txtCredsDomain ) & " domain" )
					GUICtrlSetState( $txtCredsAccount, $GUI_FOCUS )


				ElseIf ( GUICtrlRead( $txtCredsPassword ) == "" ) Then

					; GUICtrlSetData( $lblFooter, "" )
					GUICtrlSetState( $txtCredsPassword, $GUI_FOCUS )

				ElseIf ( StringInStr( GUICtrlRead( $lblFooter ), "previously authenticated", 1 ) == 0 ) Then

					GUICtrlSetData( $lblFooter, StringReplace( GUICtrlRead( $lblFooter ), "...", ":" ) & @CRLF & "Confirm password, or click Authenticate." )
					GUICtrlSetState( $txtCredsPassword, $GUI_FOCUS )

				EndIf




GUISetState( $GUI_ENABLE )

ui_Refresh( $TabCredentials )


			Return True

	EndSwitch



EndFunc

Func ui_Refresh( $control )

	Switch $control

		Case $TabRequirements

					Local $chkReqs[6] = [ 5, $chkRequirement01, $chkRequirement02, $chkRequirement03, $chkRequirement04, $chkRequirement05 ]
					Local $originalFocus = ControlGetFocus ( $Window )

						For $i = 1 To $chkReqs[0]

							; GUICtrlSetState( $chkReqs[$i], $GUI_ONTOP )
							; GUICtrlSetState( $chkReqs[$i], $GUI_ENABLE )
				; GUICtrlSetState( $chkReqs[$i], $GUI_FOCUS )
				; ControlFocus( $Window, "", $originalFocus )


			Next

		Case $TabCredentials

					Local $txtCreds[4] = [ 3, $txtCredsDomain, $txtCredsAccount, $txtCredsPassword ]
					Local $originalFocus = ControlGetFocus ( $Window )

						For $i = 1 To $txtCreds[0]

							GUICtrlSetState( $txtCreds[$i], $GUI_ONTOP )
				GUICtrlSetState( $txtCreds[$i], $GUI_FOCUS )
				ControlFocus( $Window, "", $originalFocus )


			Next

		Case Else

			Return

	EndSwitch

EndFunc

Func ui_SetFontStyles( $controls, $fontStyle )

	Local $i = 0



	For $i = 1 To $controls[0]

	  ui_SetFontStyle( $controls[$i], $fontStyle )

	Next

EndFunc


Func SetupClose()


   Local $dataHide = $UI_TAB_MAIN_PAGES[ $tabMainPageIndex ]
   Local $ctrlHide = $dataHide[ $UI_TAB_PAGE_CTRL ]

	Local $response = MsgBox( ( 4 + 32 + 256 ), $UI_WINDOW_TITLE, _
	$PRODUCT_TITLE & " " & "is not fully installed at this time." & @CRLF & @CRLF & _
	"Are you sure you want to exit the" & " " & $UI_INSTALL_NAME & "?", 0, $Window )

	If ( $response == 7 ) Then Return ; Ignore, "Exit?" response is "No"

	Switch $ctrlHide

	Case $TabCredentials
		ui_BeforeHide( $ctrlHide, $ctrlHide )

	EndSwitch

   DirRemove( $FILE_PATH_INSTALL_TEMP, 1 )

   Exit

EndFunc


Func NavRightClick()

	If( $tabMainPageIndex == 8 ) Then

		GUISetState( @SW_HIDE, $Window )


		btnCredsAuthenticateClick()

		$dbOdbcDsn = GUICtrlRead( $txtServerWeb )
		$dbOdbcDbo = GUICtrlRead( $txtServerDB )

		_InstallEverything()

   ElseIf ( WinActive( $Window ) And _
	BitAND( GUICtrlGetState( $NavRight ), $GUI_ENABLE ) ) Then
	ui_ShowTab( $tabMainPageIndex + 1 )

	EndIf

EndFunc


Func NavLeftClick()

	Local Const $TAB_DATA = $UI_TAB_MAIN_PAGES[ $tabMainPageIndex ]
	Local Const $TAB_CTRL = $TAB_DATA[ $UI_TAB_PAGE_CTRL ]

	Switch $TAB_CTRL

		Case $UI_TAB_WELCOME_DATA[ $UI_TAB_PAGE_CTRL ]

				SetupClose()

		Case Else

			   If ( WinActive( $Window ) And _
					BitAND( GUICtrlGetState( $NavLeft ), $GUI_ENABLE ) ) Then

					ui_ShowTab( $tabMainPageIndex - 1 )
				EndIf
	EndSwitch

EndFunc


Func LicenseCheckboxClick()

   If ( BitAND( GUICtrlRead( $LicenseCheckbox ), $GUI_UNCHECKED ) ) Then


	GUICtrlSetState( $NavLeft, $GUI_DEFBUTTON )
	  GUICtrlSetState( $NavRight, $GUI_DISABLE )

   Else

	  GUICtrlSetState( $NavRight, $GUI_ENABLE )
	    GUICtrlSetState( $NavRight, $GUI_DEFBUTTON )

   EndIf

EndFunc

Func RequirementCheckboxClick()

			Local $chkReqs[6] = [ 5, $chkRequirement01, $chkRequirement02, $chkRequirement03, $chkRequirement04, $chkRequirement05 ]
			Local $checkedCount = 0

			For $i = 1 to $chkReqs[0]

			   If ( BitAND( GUICtrlRead( $chkReqs[$i] ), $GUI_CHECKED ) ) Then

				   $checkedCount += 1

				EndIf

			Next

		If ( $checkedCount == $chkReqs[0] ) Then

			GUICtrlSetState( $NavRight, $GUI_ENABLE )

		Else

			GUICtrlSetState( $NavRight, $GUI_DISABLE )

		EndIf

EndFunc

Func ui_ValidateCreds()

   Local $validatedCount = 0
   Local Const $countToValidate = 3

   $validatedCount += Not ( GUICtrlRead( $txtCredsDomain ) == "" )
   $validatedCount += Not ( GUICtrlRead( $txtCredsAccount ) == "" )
   $validatedCount += Not ( GUICtrlRead( $txtCredsPassword ) == "" )

   If ( ( $validatedCount >= $countToValidate ) And BitAND( GUICtrlGetState( $btnCredsAuthenticate ), $GUI_DISABLE ) ) Then

	  GUICtrlSetState( $btnCredsAuthenticate, $GUI_ENABLE )

   ElseIf ( ( $validatedCount < $countToValidate ) And BitAND( GUICtrlGetState( $btnCredsAuthenticate ), $GUI_ENABLE ) ) Then

	  GUICtrlSetState( $btnCredsAuthenticate, $GUI_DISABLE )

   EndIf

EndFunc

Func btnCredsAuthenticateClick()

	Local $domain = StringStripWS( GUICtrlRead( $txtCredsDomain ), 3 )
	Local $account = StringStripWS( GUICtrlRead( $txtCredsAccount ), 3 )
	Local $password = GUICtrlRead( $txtCredsPassword )

	Local $roles[4] = [ 3, "Local Administrator", "IC Administrator", "DB Administrator" ]
	Local $resources[4] = [ 3, @ComputerName, GUICtrlRead( $txtServerIC ), GUICtrlRead( $txtServerWeb ) ]
	Local $txtCreds[4] = [ 3, $txtCredsDomain, $txtCredsAccount, $txtCredsPassword ]

	Local $subIndex = ( $tabMainPageIndex - 6 )
	Local $result = 0

	GUICtrlSetData( $txtCredsDomain, $domain )
	GUICtrlSetData( $txtCredsAccount, $account )

	GUISetState( $Window, @SW_DISABLE )
	GUICtrlSetState( $btnCredsAuthenticate, $GUI_HIDE )

	GUICtrlSetData( $UI_TAB_CREDENTIALS_DATA[ $UI_TAB_FOOTER_CTRL ], "Checking credentials for " & $account & "... " )

	$result = nt_auth( $domain, $account, $password, $resources[ $subIndex ], $roles [$subIndex] )

	GUICtrlSetState( $btnCredsAuthenticate, $GUI_SHOW )
	GUICtrlSetState( $Window, @SW_ENABLE )
	WinActivate( $Window )

	If ( $result == $NT_AUTH_STATUS_PASS ) Then

		GUICtrlSetData( $UI_TAB_CREDENTIALS_DATA[ $UI_TAB_FOOTER_CTRL ], $account & " credentials are valid." )
		GUICtrlSetState( $NavRight, $GUI_DEFBUTTON )
		ui_Refresh( $TabCredentials )
		Return True

	Else

		GUICtrlSetData( $UI_TAB_CREDENTIALS_DATA[ $UI_TAB_FOOTER_CTRL ], "Could not authenticate " & $account & "..." )
		GUICtrlSetState( $btnCredsAuthenticate, $GUI_DEFBUTTON )
		ui_Refresh( $TabCredentials )
		ui_AfterShow( $UI_TAB_CREDENTIALS_DATA[ $UI_TAB_PAGE_CTRL ] )

	EndIf




EndFunc

Func ui_ValidateEnv()

   Local $i = 0
   Local $validatedCount = 0
   Local Const $countToValidate = 4

   Local $txtsInfrastructure[4] = [ 3, $txtServerIC, $txtServerWEB, $txtServerDB ]
   Local $rdosEnvironment[7] = [ 6, $rdoEnvDevelopment, $rdoEnvTestingQA, $rdoEnvDemonstration, $rdoEnvTraining, $rdoEnvStage, $rdoEnvProduction ]


   For $i = 1 To $rdosEnvironment[0]

	  If ( BitAND( GUICtrlRead( $rdosEnvironment[$i] ), $GUI_CHECKED ) ) Then $validatedCount += 1

   Next

   For $i = 1 To $txtsInfrastructure[0]

	  If ( GUICtrlRead( $txtsInfrastructure[$i] ) ) Then $validatedCount += 1

   Next


   If ( BitAND( GUICtrlRead( $rdoEnvOther ), $GUI_CHECKED ) ) Then

	  If ( GUICtrlRead( $txtEnvOther ) ) Then $validatedCount += 1

   EndIf


   If ( $validatedCount >= $countToValidate And BitAND( GUICtrlGetState( $NavRight ), $GUI_DISABLE ) ) Then

	  GUICtrlSetState( $NavRight, $GUI_ENABLE )

   ElseIf ( $validatedCount < $countToValidate And BitAND( GUICtrlGetState( $NavRight ), $GUI_ENABLE ) ) Then

	  GUICtrlSetState( $NavRight, $GUI_DISABLE )

   EndIf

EndFunc


Func rdoEnvClick()

   GUICtrlSetState( $txtEnvOther, $GUI_DISABLE )

   If( GUICtrlRead( $txtServerIC ) == "" ) Then

	   GUICtrlSetState( $txtServerIC, $GUI_FOCUS )

	ElseIf( GUICtrlRead( $txtServerWEB ) == "" ) Then

		GUICtrlSetState( $txtServerWEB, $GUI_FOCUS )

	ElseIf( GUICtrlRead( $txtServerDB ) == "" ) Then

		GUICtrlSetState( $txtServerDB, $GUI_FOCUS )

	Else

		GUICtrlSetState( $NavRight, $GUI_FOCUS )

	EndIf

	ui_ValidateEnv()

EndFunc


Func rdoEnvOtherClick()

   GUICtrlSetState( $txtEnvOther, $GUI_ENABLE )
   ControlFocus ( $Window, "", $txtEnvOther )

   ui_ValidateEnv()

EndFunc

Func txtCredentialsChange()

	Local $roleNames[4] = [ 3, "Local Administrator", "IC Administrator", "DB Administrator" ]
	Local $txtCreds[4] = [ 3, GUICtrlRead( $txtCredsDomain ), GUICtrlRead( $txtCredsAccount ), GUICtrlRead( $txtCredsPassword ) ]
	Local $resources[4] = [ 3, @ComputerName, GUICtrlRead( $txtServerIC ), GUICtrlRead( $txtServerWeb ) ]

	Local $subIndex = ( $tabMainPageIndex - 6 )
	Local $credRecall = nt_recall( $resources[ $subIndex ], $roleNames[ $subIndex ] )


	If ( GUICtrlRead( $CredentialsFooter ) == "" ) Then
	Return

	ElseIf ( (  StringLower( $credRecall[ $NT_AUTH_DOMAIN_IDX ] ) <> StringLower( $txtCreds[ 1 ] ) ) Or _
		( StringLower( $credRecall[ $NT_AUTH_USER_IDX ] ) <> StringLower( $txtCreds[ 2 ] ) ) Or _
		( ( $credRecall[ $NT_AUTH_PASSWORD_IDX ] <> $txtCreds[ 3 ] ) And ( $txtCreds[ 3 ] <> "" ) ) )  Then

			; GUICtrlSetData( $CredentialsFooter, "" )

		EndIf

EndFunc

Func txtServerChange()

   ui_ValidateEnv()

EndFunc

Func AboutFooterClick()

   ShellExecute( $PRODUCT_URL_ABOUT )

EndFunc

Func EnvironmentWebLink()

	Local $launched = 0
	Local $icServer = GUICtrlRead( $txtServerIC )

	MsgBox( 64, $UI_WINDOW_TITLE, ( "Use the Windows ODBC utility to establish a data source (System DSN). Then, close the ODBC utility to continue with the " & $UI_INSTALL_NAME & "." ), 0, $Window )

	ShellExecute( "ODBCAD32.EXE", "", ( @WindowsDir & "\SysWOW64\" ) )
	$launched = WinWait( "ODBC", "", 5 )

	If ( $launched <> 0 ) Then

		WinSetOnTop( "ODBC", "", 1 )
		WinActivate( "ODBC" )

		GUICtrlSetState( $NavLeft, $GUI_DISABLE )
		GUICtrlSetState( $NavRight, $GUI_DISABLE )

		WinWaitClose( "ODBC" )

		GUICtrlSetState( $NavLeft, $GUI_ENABLE )
		GUICtrlSetState( $NavRight, $GUI_ENABLE )

		ui_ValidateEnv()
		WinActivate( $Window )


	EndIf

EndFunc
;
;
;

Dim Const $ui_accelerators[3][2] = _
[ _
	[ "{PGUP}", $NavLeft ], _
	[ "{PGDN}", $NavRight ], _
	[ "{ENTER}", $NavRight ] _
]

GUISetAccelerators( $ui_accelerators, $Window )


Dim $oldPosition = WinGetPos( $Window )

While 1

   Local $tabCurrentIndex = GUICtrlRead( $TabMain ) + 2
   Local $tabCurrentData = $UI_TAB_MAIN_PAGES[ $tabCurrentIndex ]

	Local $newPosition = WinGetPos( $Window )

	If( Not WinActive( $Window ) ) Then

		ui_Refresh( $tabCurrentData[ $UI_TAB_PAGE_CTRL ] )
		WinWaitActive( $Window )
		ui_Refresh( $tabCurrentData[ $UI_TAB_PAGE_CTRL ] )

	ElseIf ( ( $oldPosition[0] <> $newPosition[0] ) Or _
		( $oldPosition[1] <> $newPosition[1] ) ) Then

		$oldPosition = WinGetPos( $Window )
	   ui_Refresh( $tabCurrentData[ $UI_TAB_PAGE_CTRL ] )

	EndIf

   Switch $tabCurrentData[ $UI_TAB_PAGE_CTRL ]

	  Case $UI_TAB_ENVIRONMENT_DATA[ $UI_TAB_PAGE_CTRL ]

		 ui_ValidateEnv()

	  Case $UI_TAB_CREDENTIALS_DATA[ $UI_TAB_PAGE_CTRL ]

		 ui_ValidateCreds()
		 txtCredentialsChange()

	  Case Else

   EndSwitch

   Sleep( 100 )

WEnd