USE [master]
GO
/****** Object:  Database [ReferralsTransfers]    Script Date: 12/18/2013 13:16:56 ******/
CREATE DATABASE [ReferralsTransfers] ON  PRIMARY 
( NAME = N'ReferralsTransfers', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\ReferralsTransfers.mdf' , SIZE = 2048KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'ReferralsTransfers_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\ReferralsTransfers_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [ReferralsTransfers] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [ReferralsTransfers].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [ReferralsTransfers] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [ReferralsTransfers] SET ANSI_NULLS OFF
GO
ALTER DATABASE [ReferralsTransfers] SET ANSI_PADDING OFF
GO
ALTER DATABASE [ReferralsTransfers] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [ReferralsTransfers] SET ARITHABORT OFF
GO
ALTER DATABASE [ReferralsTransfers] SET AUTO_CLOSE OFF
GO
ALTER DATABASE [ReferralsTransfers] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [ReferralsTransfers] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [ReferralsTransfers] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [ReferralsTransfers] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [ReferralsTransfers] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [ReferralsTransfers] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [ReferralsTransfers] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [ReferralsTransfers] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [ReferralsTransfers] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [ReferralsTransfers] SET  DISABLE_BROKER
GO
ALTER DATABASE [ReferralsTransfers] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [ReferralsTransfers] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [ReferralsTransfers] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [ReferralsTransfers] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [ReferralsTransfers] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [ReferralsTransfers] SET READ_COMMITTED_SNAPSHOT OFF
GO
ALTER DATABASE [ReferralsTransfers] SET HONOR_BROKER_PRIORITY OFF
GO
ALTER DATABASE [ReferralsTransfers] SET  READ_WRITE
GO
ALTER DATABASE [ReferralsTransfers] SET RECOVERY FULL
GO
ALTER DATABASE [ReferralsTransfers] SET  MULTI_USER
GO
ALTER DATABASE [ReferralsTransfers] SET PAGE_VERIFY CHECKSUM
GO
ALTER DATABASE [ReferralsTransfers] SET DB_CHAINING OFF
GO
EXEC sys.sp_db_vardecimal_storage_format N'ReferralsTransfers', N'ON'
GO
USE [ReferralsTransfers]
GO
/****** Object:  Table [dbo].[Appointment]    Script Date: 12/18/2013 13:16:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Appointment](
	[Process_ID] [varchar](10) NOT NULL,
	[Logged] [datetime] NOT NULL,
	[User_Name] [nvarchar](50) NOT NULL,
	[User_Display] [nvarchar](75) NOT NULL,
	[Patient_Name] [nvarchar](75) NOT NULL,
	[Provider_Name] [nvarchar](75) NOT NULL,
	[Appt_Begin] [datetime] NOT NULL,
	[Appt_End] [datetime] NOT NULL,
	[Status] [nvarchar](50) NOT NULL,
	[Provider_Rep] [nvarchar](75) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Activity_Log]    Script Date: 12/18/2013 13:16:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Activity_Log](
	[Process_ID] [varchar](10) NOT NULL,
	[Entry_ID] [int] NOT NULL,
	[Logged] [datetime] NOT NULL,
	[Category_Type] [varchar](50) NULL,
	[User_Name] [varchar](50) NULL,
	[User_Display] [varchar](75) NULL,
	[Activity] [varchar](250) NOT NULL,
	[Entity_Name] [varchar](200) NULL,
	[Entity_Role] [varchar](50) NULL,
	[User_Notes] [text] NULL,
	[Category_Finished] [bit] NULL,
	[Link_Attachment_Path] [varchar](250) NULL,
	[Link_Attachment_Source] [varchar](250) NULL,
	[Process_State] [varchar](75) NULL,
	[Interaction_Type] [varchar](50) NULL,
	[Interaction_ID] [varchar](50) NULL,
	[Interaction_Origin] [varchar](50) NULL,
	[Interaction_Address] [varchar](75) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
