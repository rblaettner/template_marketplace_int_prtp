#RequireAdmin
;
#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_Outfile=32.exe
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****
;
;
;
	AutoItSetOption( "WinTitleMatchMode", 2 )

	Dim $progressCount = ( -1 )
	Dim Const $progressTotal = 211

;	Default file storage locations ("Resources\...")
;
	Global Const $IC_HOSTNAME = @ComputerName
	Dim Const $APP_SUBFOLDER = "ReferralsTransfers"
;
	Dim Const $IC_SERVER_DIR = RegRead( "\\" & $IC_HOSTNAME & "\" & "HKLM\SOFTWARE\Interactive Intelligence", "Target" )
	Dim Const $IC_I3_IC_PATH = RegRead( "\\" & $IC_HOSTNAME & "\" & "HKLM\SOFTWARE\Interactive Intelligence", "Value" )

	Dim Const $IC_SHARE_RESOURCES = $IC_SERVER_DIR & "Resources" & "\" & $APP_SUBFOLDER
	; Dim Const $IC_LOCAL_RESOURCES = $IC_I3_IC_PATH & "Resources" & "\" & $APP_SUBFOLDER
;

;	==========
;
;	Registry values for future lookup and reference...
;
	Dim Const $DS_ROOT_KEY = "HKLM\SOFTWARE\Interactive Intelligence\EIC\Directory Services\Root"
;
	Dim Const $DS_SERVER_VAL = RegRead( ( "\\" & $IC_HOSTNAME & "\" & $DS_ROOT_KEY ), "SERVER" )
	Dim Const $DS_CONFIG_VAL = RegRead( ( "\\" & $IC_HOSTNAME & "\" & $DS_ROOT_KEY ), "CONFIG" )
	Dim Const $DS_SITE_VALUE = RegRead( ( "\\" & $IC_HOSTNAME & "\" & $DS_ROOT_KEY ), "SITE" )
	Dim Const $DS_CONFIG_KEY = ( $DS_ROOT_KEY & $DS_CONFIG_VAL & "\" & "Configuration" )

	Local Const $DS_DATA_SOURCE = "ININ_MP_RFTR_01"

	$dbOdbcDsn = RegRead( ( "\\" & $IC_HOSTNAME & "\" & $DS_CONFIG_KEY & "\" & "Data Sources" & "\" & $DS_DATA_SOURCE ), "ODBC DSN" )
	$dbOdbcDbo = RegRead( ( "\\" & $IC_HOSTNAME & "\" & $DS_CONFIG_KEY & "\" & "Data Sources" & "\" & $DS_DATA_SOURCE ), "QUAL" )
;
;	==========
;
;	Derivation of Server, Site...
;
	Dim Const $DS_SERVER_NAME = StringReplace( StringReplace( $DS_SERVER_VAL, $DS_CONFIG_VAL, "" ), "\", "" )
	Dim Const $DS_SITE_NAME = StringReplace( $DS_SITE_VALUE, "\", "" )
;
;	=========

;	Interaction Center Version (major) and Service Update (minor)
;
	Dim Const $REG_UNINSTALL = "HKLM64\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall"
	Dim Const $IC_SERVER_REG_GUID = "{71081478-8A9A-4B4E-AE0A-0B2750E8124F}"
;
	Dim Const $IC_VERSION_MJR = Int( RegRead( ( "\\" & $IC_HOSTNAME & "\" & $REG_UNINSTALL & "\" & $IC_SERVER_REG_GUID ), "VersionMajor" ) )
	Dim Const $IC_VERSION_MNR = Int( RegRead( ( "\\" & $IC_HOSTNAME & "\" & $REG_UNINSTALL & "\" & $IC_SERVER_REG_GUID ), "VersionMinor" ) )

	Local $retValue = 0


	$retValue = ShellExecuteWait( "NET", "STOP ININ.MarketPlace.IPA.Utilities-w32r-1-0", ".", "", @SW_HIDE )
	$retValue = ShellExecuteWait( "NET", "START ININ.MarketPlace.IPA.Utilities-w32r-1-0", ".", "", @SW_HIDE )

	If( $retValue <> 0 ) Then

		MsgBox( 48 + 4096, $UI_WINDOW_TITLE, _
		"ININ MarketPlace (IPA) Utilities is required before installing " & $PRODUCT_TITLE & "." & Chr(13) & Chr(13) & _
		"Install ININ MarketPlace (IPA) Utilities, then re-run this installer." )

		Exit

	EndIf

	If ( ( ( $IC_VERSION_MJR == 4 ) And ( $IC_VERSION_MNR < 4 ) ) Or ( $IC_VERSION_MJR < 4 ) ) Then

		MsgBox( 48 + 4096, $UI_WINDOW_TITLE, _
		"The offical release version of " & $PRODUCT_TITLE & " is validated on server software for  Interaction Center 4.0,  Service Update 4,  " & _
		"or later." & Chr(13) & Chr(13) & "Refer to the accompanying documentation for more information and technical support resources." )

		Exit

	EndIf


	While ( Not ( ProcessExists( "NotifierU.EXE" ) And ProcessExists( "ProcessAutomationServerU.EXE" ) ) )

		Local $retVal = MsgBox( 5 + 48 + 4096, $UI_WINDOW_TITLE, "Interaction Center and Process Automation Server must be running to continue with installation." )

		If ( $retVal == 2 ) Then

			Exit

		EndIf

	WEnd

	While ( ProcessExists( "IDU.EXE" ) )

		Local $retVal = MsgBox( 5 + 48 + 4096, $UI_WINDOW_TITLE, "Close Interaction Designer (IDU.EXE, for all users) before continuing with installation." )

		If ( $retVal == 2 ) Then

			Exit

		EndIf

	WEnd


	MsgBox( 64 + 4096, $UI_WINDOW_TITLE, "This installer can simplify some aspects of " & $PRODUCT_TITLE & " setup and configuration. " & _
	"After installation, refer to the solution's documentation to verify automated setup activities and for further customization options." )

;

;	==========

	Func _InstallEverything()

		Local Const $FILE_PATH_CONFIG_I3DIRSVCS = $FILE_PATH_INSTALL_TEMP & "\IC\I3DIRSVCS" & "\"
		Local Const $FILE_PATH_CONFIG_IPAEXPORT = $FILE_PATH_INSTALL_TEMP & "\IC\IPAEXPORT" & "\"

		Local Const $FILE_PATH_CONFIG_INTXHNDLR = $IC_I3_IC_PATH & "Handlers\Custom\" & $APP_SUBFOLDER & "\"

		Local Const $FILE_PATH_CONFIG_TMPL = $IC_SHARE_RESOURCES & "\" & "TMPL" & "\"
		Local Const $FILE_PATH_CONFIG_TMPL_EML = $FILE_PATH_CONFIG_TMPL & "\" & "EML" & "\"
		Local Const $FILE_PATH_CONFIG_TMPL_TXT = $FILE_PATH_CONFIG_TMPL & "\" & "TXT" & "\"
		Local Const $FILE_PATH_CONFIG_TMPL_HTM = $FILE_PATH_CONFIG_TMPL & "\" & "HTM" & "\"
		Local Const $FILE_PATH_CONFIG_TMPL_DOC = $FILE_PATH_CONFIG_TMPL & "\" & "DOC" & "\"

		Local Const $FILE_PATH_CONFIG_TMPL_HTM_LOG = $FILE_PATH_CONFIG_TMPL_HTM & "\" & "LOG" & "\"

		Local Const $FILE_PATH_CONFIG_TMPL_HTM_LOG_IMAGES = $FILE_PATH_CONFIG_TMPL_HTM_LOG & "\" & "IMAGES" & "\"
		Local Const $FILE_PATH_CONFIG_TMPL_HTM_LOG_STYLES = $FILE_PATH_CONFIG_TMPL_HTM_LOG & "\" & "STYLES" & "\"

		Local Const $FILE_PATH_CONFIG_INTX = $IC_SHARE_RESOURCES & "\" & "INTX" & "\"
		Local Const $FILE_PATH_CONFIG_INTX_EML = $FILE_PATH_CONFIG_INTX & "\" & "EML" & "\"
		Local Const $FILE_PATH_CONFIG_INTX_FAX = $FILE_PATH_CONFIG_INTX & "\" & "FAX" & "\"
		Local Const $FILE_PATH_CONFIG_INTX_TXT = $FILE_PATH_CONFIG_INTX & "\" & "TXT" & "\"

		Local Const	$FILE_PATH_CONFIG_DOCREF = $IC_SHARE_RESOURCES & "\" & "Reference" & "\"
		Local Const	$FILE_PATH_CONFIG_WEBSVC = $FILE_PATH_CONFIG_DOCREF & "\" & "Web Service" & "\"
		Local Const	$FILE_PATH_CONFIG_SQLDOC = $FILE_PATH_CONFIG_DOCREF & "\" & "Database Setup" & "\"

		Local Const	$FILE_PATH_CONFIG_WI = $IC_SHARE_RESOURCES & "\" & "WI"
		Local Const $FILE_PATH_CONFIG_WI_IMG = $FILE_PATH_CONFIG_WI & "\" & "IMG" & "\"
		Local Const $FILE_PATH_CONFIG_WI_LOG =  $FILE_PATH_CONFIG_WI & "\" & "LOG" & "\"

		Local Const $FILE_PATH_CONFIG_WI_LOG_IMAGES = $FILE_PATH_CONFIG_WI_LOG & "\" & "IMAGES" & "\"
		Local Const $FILE_PATH_CONFIG_WI_LOG_STYLES = $FILE_PATH_CONFIG_WI_LOG & "\" & "STYLES" & "\"

		Local Const $FILE_PATH_CONFIG_PDF =  $IC_SHARE_RESOURCES & "\" & "PDF" & "\"
		Local Const $FILE_PATH_CONFIG_PDF_MFI = $FILE_PATH_CONFIG_PDF & "\" & "MFI" & "\"
		Local Const $FILE_PATH_CONFIG_PDF_DAT = $FILE_PATH_CONFIG_PDF & "\" & "DAT" & "\"
		Local Const $FILE_PATH_CONFIG_PDF_GEN = $FILE_PATH_CONFIG_PDF & "\" & "GEN" & "\"



		_InstallProgress( 0, "Creating Directories" )  ;  ==========

		DirCreate( $FILE_PATH_CONFIG_I3DIRSVCS )
		DirCreate( $FILE_PATH_CONFIG_IPAEXPORT )
		DirCreate( $FILE_PATH_CONFIG_INTXHNDLR )

		DirCreate( $IC_SHARE_RESOURCES )

		DirCreate( $FILE_PATH_CONFIG_TMPL )
		DirCreate( $FILE_PATH_CONFIG_TMPL_EML )
		DirCreate( $FILE_PATH_CONFIG_TMPL_TXT )
		DirCreate( $FILE_PATH_CONFIG_TMPL_HTM )
		DirCreate( $FILE_PATH_CONFIG_TMPL_DOC )

		DirCreate( $FILE_PATH_CONFIG_TMPL_HTM_LOG )

		DirCreate( $FILE_PATH_CONFIG_TMPL_HTM_LOG_IMAGES )
		DirCreate( $FILE_PATH_CONFIG_TMPL_HTM_LOG_STYLES )

		DirCreate( $FILE_PATH_CONFIG_INTX )
		DirCreate( $FILE_PATH_CONFIG_INTX_EML )
		DirCreate( $FILE_PATH_CONFIG_INTX_FAX )
		DirCreate( $FILE_PATH_CONFIG_INTX_TXT )

		DirCreate( $FILE_PATH_CONFIG_DOCREF )
		DirCreate( $FILE_PATH_CONFIG_WEBSVC )
		DirCreate( $FILE_PATH_CONFIG_SQLDOC )

		DirCreate( $FILE_PATH_CONFIG_WI )

		DirCreate( $FILE_PATH_CONFIG_WI_IMG )
		DirCreate( $FILE_PATH_CONFIG_WI_LOG )
		DirCreate( $FILE_PATH_CONFIG_WI_LOG_IMAGES )
		DirCreate( $FILE_PATH_CONFIG_WI_LOG_STYLES )

		DirCreate( $FILE_PATH_CONFIG_PDF )
		DirCreate( $FILE_PATH_CONFIG_PDF_MFI )
		DirCreate( $FILE_PATH_CONFIG_PDF_DAT )
		DirCreate( $FILE_PATH_CONFIG_PDF_GEN )

		_InstallProgress( 0, "Copying Templates" )  ;  ==========

		FileInstall( "C:\I3\IC\Server\Resources\ReferralsTransfers\TMPL\EML\Patient - Approved.HTML", $FILE_PATH_CONFIG_TMPL_EML, 1 )

		_InstallProgress()

		FileInstall( "C:\I3\IC\Server\Resources\ReferralsTransfers\TMPL\EML\Referred To Provider - Patient Approved.HTML", $FILE_PATH_CONFIG_TMPL_EML, 1 )

		_InstallProgress()

		FileInstall( "C:\I3\IC\Server\Resources\ReferralsTransfers\TMPL\EML\Text Message Demo.HTML", $FILE_PATH_CONFIG_TMPL_EML, 1 )

		_InstallProgress()

		FileInstall( "C:\I3\IC\Server\Resources\ReferralsTransfers\TMPL\EML\Basic Message.HTML", $FILE_PATH_CONFIG_TMPL_EML, 1 )

		_InstallProgress()

		FileInstall( "C:\I3\IC\Server\Resources\ReferralsTransfers\TMPL\HTM\LOG\Append.HTML", $FILE_PATH_CONFIG_TMPL_HTM_LOG, 1 )

		_InstallProgress()

		FileInstall( "C:\I3\IC\Server\Resources\ReferralsTransfers\TMPL\HTM\LOG\Data.HTML", $FILE_PATH_CONFIG_TMPL_HTM_LOG, 1 )

		_InstallProgress()

		FileInstall( "C:\I3\IC\Server\Resources\ReferralsTransfers\TMPL\HTM\LOG\No-Data.HTML", $FILE_PATH_CONFIG_TMPL_HTM_LOG, 1 )

		_InstallProgress()

		FileInstall( "C:\I3\IC\Server\Resources\ReferralsTransfers\TMPL\HTM\LOG\Prepend.HTML", $FILE_PATH_CONFIG_TMPL_HTM_LOG, 1 )

		_InstallProgress()

		FileInstall( "C:\I3\IC\Server\Resources\ReferralsTransfers\TMPL\HTM\LOG\Source.HTML", $FILE_PATH_CONFIG_TMPL_HTM_LOG, 1 )

		_InstallProgress()

		FileInstall( "C:\I3\IC\Server\Resources\ReferralsTransfers\TMPL\HTM\LOG\STYLES\Main.CSS", $FILE_PATH_CONFIG_TMPL_HTM_LOG_STYLES, 1 )

		_InstallProgress()

		FileInstall( "C:\I3\IC\Server\Resources\ReferralsTransfers\TMPL\HTM\LOG\STYLES\No-Data.CSS", $FILE_PATH_CONFIG_TMPL_HTM_LOG_STYLES, 1 )

		_InstallProgress()

		FileInstall( "C:\I3\IC\Server\Resources\ReferralsTransfers\TMPL\TXT\Patient - Approved.TXT", $FILE_PATH_CONFIG_TMPL_TXT, 1 )

		_InstallProgress()

		FileInstall( "C:\I3\IC\Server\Resources\ReferralsTransfers\TMPL\DOC\Basic Fax.DOC", $FILE_PATH_CONFIG_TMPL_DOC, 1 )

		_InstallProgress()

		_InstallProgress( 0, "Copying Fax Generator" )  ;  ==========

		FileInstall( "C:\I3\IC\Server\Resources\ReferralsTransfers\PDF\MFI\ace-w32r-1-1.dll", $FILE_PATH_CONFIG_PDF_MFI, 1 )

		_InstallProgress()

		FileInstall( "C:\I3\IC\Server\Resources\ReferralsTransfers\PDF\MFI\i3core-w32r-1-1.dll", $FILE_PATH_CONFIG_PDF_MFI, 1 )

		_InstallProgress()

		FileInstall( "C:\I3\IC\Server\Resources\ReferralsTransfers\PDF\MFI\i3moduleregistry-w32.dll", $FILE_PATH_CONFIG_PDF_MFI, 1 )

		_InstallProgress()

		FileInstall( "C:\I3\IC\Server\Resources\ReferralsTransfers\PDF\MFI\i3trace_dotnet_tracing-w32r-1-1.dll", $FILE_PATH_CONFIG_PDF_MFI, 1 )

		_InstallProgress()

		FileInstall( "C:\I3\IC\Server\Resources\ReferralsTransfers\PDF\MFI\i3trace-w32r-1-1.dll", $FILE_PATH_CONFIG_PDF_MFI, 1 )

		_InstallProgress()

		FileInstall( "C:\I3\IC\Server\Resources\ReferralsTransfers\PDF\MFI\ININ.IceLib.dll", $FILE_PATH_CONFIG_PDF_MFI, 1 )

		_InstallProgress()

		FileInstall( "C:\I3\IC\Server\Resources\ReferralsTransfers\PDF\MFI\ININ.IceLib.pdb", $FILE_PATH_CONFIG_PDF_MFI, 1 )

		_InstallProgress()

		FileInstall( "C:\I3\IC\Server\Resources\ReferralsTransfers\PDF\MFI\ININ.IceLib.xml", $FILE_PATH_CONFIG_PDF_MFI, 1 )

		_InstallProgress()

		FileInstall( "C:\I3\IC\Server\Resources\ReferralsTransfers\PDF\MFI\ININ.ThinBridge.dll", $FILE_PATH_CONFIG_PDF_MFI, 1 )

		_InstallProgress()

		FileInstall( "C:\I3\IC\Server\Resources\ReferralsTransfers\PDF\MFI\ININ.ThinBridge.pdb", $FILE_PATH_CONFIG_PDF_MFI, 1 )

		_InstallProgress()

		FileInstall( "C:\I3\IC\Server\Resources\ReferralsTransfers\PDF\MFI\ININ.ThinBridge.Types.dll", $FILE_PATH_CONFIG_PDF_MFI, 1 )

		_InstallProgress()

		FileInstall( "C:\I3\IC\Server\Resources\ReferralsTransfers\PDF\MFI\ININ.ThinBridge.Types.pdb", $FILE_PATH_CONFIG_PDF_MFI, 1 )

		_InstallProgress()

		FileInstall( "C:\I3\IC\Server\Resources\ReferralsTransfers\PDF\MFI\Interop.Sspi.dll", $FILE_PATH_CONFIG_PDF_MFI, 1 )

		_InstallProgress()

		FileInstall( "C:\I3\IC\Server\Resources\ReferralsTransfers\PDF\MFI\Interop.Sspi.pdb", $FILE_PATH_CONFIG_PDF_MFI, 1 )

		_InstallProgress()

		FileInstall( "C:\I3\IC\Server\Resources\ReferralsTransfers\PDF\MFI\Merge Field Inspector.exe", $FILE_PATH_CONFIG_PDF_MFI, 1 )

		_InstallProgress()

		FileInstall( "C:\I3\IC\Server\Resources\ReferralsTransfers\PDF\MFI\Merge Field Inspector.exe.config", $FILE_PATH_CONFIG_PDF_MFI, 1 )

		_InstallProgress()

		FileInstall( "C:\I3\IC\Server\Resources\ReferralsTransfers\PDF\MFI\openssl-w32r-1-1.dll", $FILE_PATH_CONFIG_PDF_MFI, 1 )

		_InstallProgress()

		FileInstall( "C:\I3\IC\Server\Resources\ReferralsTransfers\PDF\MFI\Sspi_CAPIA.dll", $FILE_PATH_CONFIG_PDF_MFI, 1 )

		_InstallProgress()

		FileInstall( "C:\I3\IC\Server\Resources\ReferralsTransfers\PDF\MFI\stlport-w32r-1-1.dll", $FILE_PATH_CONFIG_PDF_MFI, 1 )

		_InstallProgress()

		FileInstall( "C:\I3\IC\Server\Resources\ReferralsTransfers\PDF\MFI\i3trace_dotnet_tracing_interop-w32r-1-1.dll", $FILE_PATH_CONFIG_PDF_MFI, 1 )

		_InstallProgress()

		_InstallProgress( 0, "Copying Configuration" )  ;  ==========

		FileInstall( "..\..\Structured Parameters.XML", $FILE_PATH_CONFIG_I3DIRSVCS, 1 )

		_InstallProgress()

		FileInstall( "..\..\Data Sources.XML", $FILE_PATH_CONFIG_I3DIRSVCS, 1 )


		_InstallProgress( 0, "Applying Configuration" )  ;  ==========

		_InstallDirectoryServices( $FILE_PATH_CONFIG_I3DIRSVCS & "Structured Parameters.XML" )

		_InstallProgress()

		_InstallDirectoryServices( $FILE_PATH_CONFIG_I3DIRSVCS & "Data Sources.XML" )


		_InstallProgress( 0, "Copying Handlers" )  ;  ==========

		FileInstall( "C:\I3\IC\Handlers\Custom\ReferralsTransfers\ReferralsTransfers_CreateLogEntry.ihd", $FILE_PATH_CONFIG_INTXHNDLR, 1 )

		_InstallProgress()

		FileInstall( "C:\I3\IC\Handlers\Custom\ReferralsTransfers\ReferralsTransfers_DeleteFile.ihd", $FILE_PATH_CONFIG_INTXHNDLR, 1 )

		_InstallProgress()

		FileInstall( "C:\I3\IC\Handlers\Custom\ReferralsTransfers\ReferralsTransfers_ListFolder.ihd", $FILE_PATH_CONFIG_INTXHNDLR, 1 )

		_InstallProgress()

		FileInstall( "C:\I3\IC\Handlers\Custom\ReferralsTransfers\ReferralsTransfers_PlaceCall.ihd", $FILE_PATH_CONFIG_INTXHNDLR, 1 )

		_InstallProgress()

		FileInstall( "C:\I3\IC\Handlers\Custom\ReferralsTransfers\ReferralsTransfers_ReadTextFile.ihd", $FILE_PATH_CONFIG_INTXHNDLR, 1 )

		_InstallProgress()

		FileInstall( "C:\I3\IC\Handlers\Custom\ReferralsTransfers\ReferralsTransfers_SaveFile.ihd", $FILE_PATH_CONFIG_INTXHNDLR, 1 )

		_InstallProgress()

		FileInstall( "C:\I3\IC\Handlers\Custom\ReferralsTransfers\ReferralsTransfers_SendFax.ihd", $FILE_PATH_CONFIG_INTXHNDLR, 1 )

		_InstallProgress()

		FileInstall( "C:\I3\IC\Handlers\Custom\ReferralsTransfers\ReferralsTransfers_MergeFax.ihd", $FILE_PATH_CONFIG_INTXHNDLR, 1 )

		_InstallProgress()

		FileInstall( "C:\I3\IC\Handlers\Custom\ReferralsTransfers\ReferralsTransfers_StructParam.ihd", $FILE_PATH_CONFIG_INTXHNDLR, 1 )

		_InstallProgress()

		FileInstall( "C:\I3\IC\Handlers\Custom\ReferralsTransfers\ReferralsTransfers_WriteLogEntry.ihd", $FILE_PATH_CONFIG_INTXHNDLR, 1 )

		_InstallProgress()

		FileCreateShortcut( $FILE_PATH_CONFIG_INTXHNDLR, $FILE_PATH_CONFIG_DOCREF & "Solution Handlers.LNK" )

		_InstallProgress()

		FileInstall( "..\..\Handlers\Handlers.LST", $FILE_PATH_CONFIG_INTXHNDLR, 1 )


		_InstallProgress( 0, "Publishing Handlers" )  ;  ==========

		_InstallHandlers( "Handlers.LST", $FILE_PATH_CONFIG_INTXHNDLR )


		_InstallProgress( 0, "Copying Images" )  ;  ==========

		FileInstall( "C:\I3\IC\Server\Resources\ReferralsTransfers\WI\IMG\Call-Invalid.PNG", $FILE_PATH_CONFIG_WI_IMG, 1 )

		_InstallProgress()

		FileInstall( "C:\I3\IC\Server\Resources\ReferralsTransfers\WI\IMG\Call-Later.PNG", $FILE_PATH_CONFIG_WI_IMG, 1 )

		_InstallProgress()

		FileInstall( "C:\I3\IC\Server\Resources\ReferralsTransfers\WI\IMG\Call-Message.PNG", $FILE_PATH_CONFIG_WI_IMG, 1 )

		_InstallProgress()

		FileInstall( "C:\I3\IC\Server\Resources\ReferralsTransfers\WI\IMG\Call-Patient.PNG", $FILE_PATH_CONFIG_WI_IMG, 1 )

		_InstallProgress()

		FileInstall( "C:\I3\IC\Server\Resources\ReferralsTransfers\WI\IMG\Logo-01.PNG", $FILE_PATH_CONFIG_WI_IMG, 1 )

		_InstallProgress()

		FileInstall( "C:\I3\IC\Server\Resources\ReferralsTransfers\WI\IMG\Send-Envelope.PNG", $FILE_PATH_CONFIG_WI_IMG, 1 )

		_InstallProgress()

		FileInstall( "C:\I3\IC\Server\Resources\ReferralsTransfers\WI\IMG\Send-Fax.PNG", $FILE_PATH_CONFIG_WI_IMG, 1 )

		_InstallProgress()

		FileInstall( "C:\I3\IC\Server\Resources\ReferralsTransfers\WI\LOG\STYLES\Main.CSS", $FILE_PATH_CONFIG_WI_LOG_STYLES, 1 )

		_InstallProgress()

		FileInstall( "C:\I3\IC\Server\Resources\ReferralsTransfers\WI\LOG\STYLES\No-Data.CSS", $FILE_PATH_CONFIG_WI_LOG_STYLES, 1 )

		_InstallProgress()





		_InstallProgress( 0, "Copying Processes" )  ;  ==========




		FileInstall( "..\..\IPA\ReferralsTransfers Email.IPAExport", $FILE_PATH_CONFIG_IPAEXPORT, 1 )

		_InstallProgress()

		FileInstall( "..\..\IPA\ReferralsTransfers Fax.IPAExport", $FILE_PATH_CONFIG_IPAEXPORT, 1 )

		_InstallProgress()

		FileInstall( "..\..\IPA\ReferralsTransfers Text.IPAExport", $FILE_PATH_CONFIG_IPAEXPORT, 1 )

		_InstallProgress()

		FileInstall( "..\..\IPA\ReferralsTransfers Console.IPAExport", $FILE_PATH_CONFIG_IPAEXPORT, 1 )

		_InstallProgress()

		FileInstall( "..\..\IPA\ReferralsTransfers Search.IPAExport", $FILE_PATH_CONFIG_IPAEXPORT, 1 )

		_InstallProgress()

		FileInstall( "..\..\IPA\New Referral or Transfer.IPAExport", $FILE_PATH_CONFIG_IPAEXPORT, 1 )

		_InstallProgress()


		_InstallProgress( 0, "Importing Processes" )  ;  ==========

		_InstallProcess( "ReferralsTransfers Email.IPAExport", $FILE_PATH_CONFIG_IPAEXPORT )

		_InstallProgress(12)

		_InstallProcess( "ReferralsTransfers Fax.IPAExport", $FILE_PATH_CONFIG_IPAEXPORT )

		_InstallProgress(12)

		_InstallProcess( "ReferralsTransfers Text.IPAExport", $FILE_PATH_CONFIG_IPAEXPORT )

		_InstallProgress(12)

		_InstallProcess( "ReferralsTransfers Console.IPAExport", $FILE_PATH_CONFIG_IPAEXPORT )

		_InstallProgress(12)

		_InstallProcess( "ReferralsTransfers Search.IPAExport", $FILE_PATH_CONFIG_IPAEXPORT )

		_InstallProgress(12)

		_InstallProcess( "New Referral or Transfer.IPAExport", $FILE_PATH_CONFIG_IPAEXPORT )

		_InstallProgress(12)

		FileCreateShortcut( $FILE_PATH_CONFIG_IPAEXPORT, ( $FILE_PATH_CONFIG_DOCREF & "Process Templates.LNK" ) )

		_InstallProgress()



		_InstallProgress( "Copying Web Service" )  ;  ==========

		FileInstall( "..\..\WEB\ININ.MarketPlace.Utilities_v2.exe", $FILE_PATH_CONFIG_WEBSVC )

		_InstallProgress()



		_InstallProgress( 0, "Creating Shortcuts" )  ;  ==========

		FileCreateShortcut( "%WINDIR%\SysWOW64\ODBCAD32.EXE", ( $FILE_PATH_CONFIG_SQLDOC & "Windows Data Sources.LNK" ) )

		_InstallProgress()

		FileCreateShortcut( "IASHELLU.EXE", ( $FILE_PATH_CONFIG_SQLDOC & "IC Data Sources.LNK" ) )

		_InstallProgress()



		_InstallProgress( 0, "Copying Documentation" )  ;  ==========

		FileInstall( "..\..\DOC\Installation Guide.PDF", $FILE_PATH_CONFIG_DOCREF, 1 )

		_InstallProgress()

		FileInstall( "..\..\DOC\Interaction Administrator Guide.PDF", $FILE_PATH_CONFIG_DOCREF, 1 )

		_InstallProgress()



		_InstallProgress()  ;  ======================================

		MsgBox( 64 + 4096, $UI_WINDOW_TITLE, "The automated setup activities for " & $PRODUCT_TITLE & " are complete." & @CRLF & @CRLF & _
		"Refer to the solution's documentation to verify the applied settings and for further customization options." & @CRLF & @CRLF & _
		"Operations: " & Int( $progressCount ) )

		ShellExecute( "EXPLORER.EXE", ( "/e /root," & Chr( 34 ) & $FILE_PATH_CONFIG_DOCREF & Chr( 34 ) ) )

		Exit


	EndFunc

	Func _InstallProgress( $factor = 1, $subText = "" )

		$progressCount += $factor
		$progressPercent = ( $progressCount / $progressTotal ) * 100


		If ( $progressCount == 0 ) Then

			ProgressOn( $PRODUCT_TITLE, ( "Installing on " & $IC_HOSTNAME ), "", -1, -1, 16 )

		ElseIf ( $progressCount == $progressTotal ) Then

			ProgressOff()
			Return

		EndIf


		If ( $subText <> "" ) Then

			ProgressSet( $progressPercent, $subText & "..." )

		Else

			ProgressSet( $progressPercent )

		EndIf



	EndFunc


	Func _InstallHandlers( $fileListName, $fileWorkingFolder )

		Local $resource = $IC_HOSTNAME
		Local $roleName = "IC Administrator"

		Local $credRecall = nt_recall( $resource, $roleName )
		Local $userName = $credRecall[ $NT_AUTH_USER_IDX ]
		Local $userPass = $credRecall[ $NT_AUTH_PASSWORD_IDX ]

		Local $fileSingleName = StringReplace( $fileListName, ".LST", ".DAT" )
		Local $outFileName = $fileWorkingFolder & $fileSingleName

		Local $in = FileOpen( ( $fileWorkingFolder & $fileListName ), 0 )
		Local $status = 0


		While ( $status == 0 )

			Local $published = 0
			Local $response = 4
			Local $attempts = 0
			Local $displayMode = @SW_HIDE

			Local $out = FileOpen( ( $fileWorkingFolder & $fileSingleName ), 2 )
			Local $line = FileReadLine( $in )

			$status = @error

			FileWriteLine( $out, $line )
			FileClose( $out )

			While ( ( $response == 4 ) And ( $published == 0 ) )

				ShellExecute( "IDU.EXE", _
					"/notifier=" & $resource & " " & _
					"/user=" & $userName & " " & _
					"/password=" & $userPass & " " & _
					"/publish:" & $outFileName, _
					$fileWorkingFolder, "", $displayMode )

				$attempts += 1

				$published = ProcessWaitClose( "IDU.EXE", 30 )

				If ( $published <> 1 ) Then

					ProcessClose( "IDU.EXE" )

					$response =	MsgBox( 2 + 48 + 256 + 262144, $UI_WINDOW_TITLE, "The following handler did not import or publish as expected:" & _
					@CRLF & @CRLF & $line & @CRLF & @CRLF & "Attempts: " & $attempts )

					$displayMode = @SW_SHOWNORMAL

				EndIf

				If ( $response == 5 ) Then

					MsgBox( 48 + 262144, $UI_WINDOW_TITLE, "After installation completes, the following handler " & _
					"will require importing and publishing within Interaction Designer:" & @CRLF & @CRLF & $line )

				ElseIf ( $response == 3 ) Then

					Exit

				EndIf

			WEnd


			If( $published == 0 ) Then

				MsgBox( 48 + 4096, $UI_WINDOW_TITLE, "After installation completes, the following handler " & _
				"will require importing and publishing within Interaction Designer:" & @CRLF & @CRLF & $line )

			EndIf

			_InstallProgress(6)

		WEnd

		FileClose( $in )
		FileDelete( $fileSingleName )

		;EICPublisherU.EXE

	EndFunc

	Func _InstallProcess( $fileProcess, $fileWorkingFolder )

		Local Const $NT_AUTH_STATUS_IDX = 0
		Local Const $NT_AUTH_DOMAIN_IDX = 1
		Local Const $NT_AUTH_USER_IDX = 2
		Local Const $NT_AUTH_PASSWORD_IDX = 3
		Local Const $NT_AUTH_RECORDED_IDX = 4

		Local $resource = $IC_HOSTNAME
		Local $roleName = "IC Administrator"
		Local $credRecall = nt_recall( $resource, $roleName )
		Local $userName = $credRecall[ $NT_AUTH_USER_IDX ]
		Local $userPass = $credRecall[ $NT_AUTH_PASSWORD_IDX ]

		Local $published = 0
		Local $response = 4
		Local $attempts = 0
		Local $displayMode = @SW_HIDE

		While ( ( $response == 4 ) And ( $published == 0 ) )

			ShellExecute( "FlowUtil.EXE",  _
				"/user "  &  $userName  & " /password "  &  $userPass  & " " & _
				"/server "  &  $IC_HOSTNAME  &  " /import "  & _
				Chr(34) & $fileProcess & Chr(34) & " " & _
				"/publish",  $fileWorkingFolder,  "", $displayMode )

				$attempts += 1

			$published = ProcessWaitClose( "FlowUtil.EXE", 60 )

			If ( $published == 1 ) Then

				Return

			Else

				ProcessClose( "FlowUtil.EXE" )

				$response =	MsgBox( 2 + 48 + 256 + 262144, $UI_WINDOW_TITLE, "The following process did not import or publish as expected:" & _
				@CRLF & @CRLF & $fileProcess & @CRLF & @CRLF & "Attempts: " & $attempts )

				$displayMode = @SW_SHOWNORMAL

			EndIf

		WEnd

		If ( $response == 5 ) Then

			MsgBox( 48 + 262144, $UI_WINDOW_TITLE, "After installation completes, the following process " & _
			"will require importing and publishing within IPA Designer:" & @CRLF & @CRLF & $fileProcess )

		ElseIf ( $response == 3 ) Then

			Exit

		EndIf

	EndFunc


	Func _InstallDirectoryServices( $filePathIn )

		Local $filePathOut = StringReplace( $filePathIn, ".XML", ".DAT" )

		Local $in = FileOpen( $filePathIn, 0 )
		Local $out = FileOpen( $filePathOut, 2 )

		Local $clipTemp = ""

		Local $line = ""
		Local $status = 0

		While ( $status == 0 )

			$line = FileReadLine( $in )
			$status = @error

			$line = StringReplace( $line, "__:SERVER:__", $DS_SERVER_NAME )
			$line = StringReplace( $line, "__:SITE:__", $DS_SITE_NAME )

			$line = StringReplace( $line, "__::I3_IC_PATH::__", $IC_I3_IC_PATH  )

			$line = StringReplace( $line, "__:HOSTNAME:__", $IC_HOSTNAME )
			$line = StringReplace( $line, "__:ODBC-DSN:__", $dbOdbcDsn )
			$line = StringReplace( $line, "__:ODBC-DBO:__", $dbOdbcDbo )

			FileWriteLine( $out, $line )

		WEnd

		FileClose( $filePathIn )
		FileClose( $filePathOut )

		While WinExists( "DSEdit" )

			WinClose( "DSEdit" )

		WEnd

		BlockInput( 1 )

		Run( "DSEDITU.EXE", "",  @SW_HIDE)
		WinWait( "DSEdit" )

		_InstallProgress()

		WinClose( "DSEdit" )
		WinWaitClose( "DSEdit" )

		_InstallProgress()


		Run( "DSEDITU.EXE", "",  @SW_HIDE)
		WinWait( "DSEdit" )

		_InstallProgress()

		WinActivate( "DSEdit" )
		WinWaitActive( "DSEdit" )
		Send( "!f{ENTER}" )

		WinWait( "Open" )
		WinSetState( "Open", "", @SW_HIDE)
		WinActivate( "Open" )
		WinWaitActive( "Open" )

		$clipTemp = ClipGet()
		ClipPut( Chr(34) & $filePathOut & Chr(34) )
		Send( "^v{ENTER}" )
		ClipPut( $clipTemp )

		WinWaitActive( "DSEdit" )
		WinClose( "DSEdit" )

		BlockInput( 0 )

	EndFunc

;