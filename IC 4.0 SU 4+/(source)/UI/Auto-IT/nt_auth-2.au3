#include <Crypt.au3>
#include <Security.au3>
#include <Array.au3>
#include <Date.au3>

Local Const $REG_ROOT_PATH = "HKCU\Software\Interactive Intelligence\MarketPlace\Strategic Initiatives\" & $PRODUCT_TITLE & "\" & $PRODUCT_VERSION & "\Installer\Credentials"
Local Const $TEST_COMMAND = "WHOAMI.EXE"

Local Const $CRYPTO_SYMMETRIC_ALGORITHM = $CALG_AES_256
Local Const $CRYPTO_HASH_ALGORITHM = $CALG_SHA1

Global Const $NT_AUTH_STATUS_IDX = 0
Global Const $NT_AUTH_DOMAIN_IDX = 1
Global Const $NT_AUTH_USER_IDX = 2
Global Const $NT_AUTH_PASSWORD_IDX = 3
Global Const $NT_AUTH_RECORDED_IDX = 4

Global Const $NT_AUTH_STATUS_PASS = "+1"
Global Const $NT_AUTH_STATUS_FAIL = "0"
Global Const $NT_AUTH_STATUS_NONE = "-1"
Global Const $NT_AUTH_STATUS_EXPR = "-2"
Global Const $NT_AUTH_STATUS_SAVE = "-3"

Func nt_RegPath( Const $RESOURCE_NAME, Const $RESOURCE_ROLE )

	Return ( $REG_ROOT_PATH & "\" &  $RESOURCE_NAME & "\" & $RESOURCE_ROLE )

EndFunc

Func nt_recall( Const $RESOURCE_NAME, Const $RESOURCE_ROLE )

	Dim $result[4]

	Local Const $REG_ROOT_KEY_PATH = nt_RegPath( $RESOURCE_NAME, $RESOURCE_ROLE )

 	Local Const $REG_NT_DOMAIN = RegRead( $REG_ROOT_KEY_PATH, "Domain" )
	Local Const $REG_NT_USER = RegRead( $REG_ROOT_KEY_PATH, "User" )
	Local $ntPassword = RegRead( $REG_ROOT_KEY_PATH, "Password" )

	Local Const $REG_AUTH_RESULT = RegRead( $REG_ROOT_KEY_PATH, "Status" )
	Local Const $REG_DATE_TIME = RegRead( $REG_ROOT_KEY_PATH, "Recorded" )

	Local Const $REG_AUTH_NONCE = RegRead( $REG_ROOT_KEY_PATH, "Nonce" )
	Local Const $REG_AUTH_SIGNATURE = RegRead( $REG_ROOT_KEY_PATH, "Signature" )

	Local Const $CRYPTO_PASSWORD = nt_CryptoPassword( $REG_AUTH_NONCE )
	Local Const $CRYPTO_HASH_SALT = $CRYPTO_PASSWORD

	If ( $REG_AUTH_SIGNATURE <> nt_hash( ( "" & $REG_NT_DOMAIN & $REG_NT_USER & $ntPassword & $REG_DATE_TIME & $REG_AUTH_RESULT ), $CRYPTO_HASH_SALT ) ) Then

		Dim $result[5] = [ $NT_AUTH_STATUS_NONE, "", "", "", "" ]
		Return $result

	ElseIf( nt_decrypt( $ntPassword, $CRYPTO_PASSWORD ) == $CRYPTO_HASH_SALT )  Then

		$ntPassword = ""

	Else

		$ntPassword = nt_decrypt( $ntPassword, $CRYPTO_PASSWORD )

	EndIf


	Switch ( nt_decrypt( $REG_AUTH_RESULT, $CRYPTO_PASSWORD ) )

		Case $NT_AUTH_STATUS_FAIL

		Dim $result[5] = [ $NT_AUTH_STATUS_FAIL, nt_decrypt( $REG_NT_DOMAIN, $CRYPTO_PASSWORD ), nt_decrypt( $REG_NT_USER, $CRYPTO_PASSWORD ), $ntPassword,  nt_decrypt( $REG_DATE_TIME, $CRYPTO_PASSWORD ) ]

	Case $NT_AUTH_STATUS_SAVE

		Dim $result[5] = [ $NT_AUTH_STATUS_SAVE, nt_decrypt( $REG_NT_DOMAIN, $CRYPTO_PASSWORD ), nt_decrypt( $REG_NT_USER, $CRYPTO_PASSWORD ), $ntPassword, nt_decrypt( $REG_DATE_TIME, $CRYPTO_PASSWORD )  ]

	Case $NT_AUTH_STATUS_PASS

		; Local $process = nt_auth( nt_decrypt( $REG_NT_DOMAIN, $CRYPTO_PASSWORD ), nt_decrypt( $REG_NT_USER, $CRYPTO_PASSWORD ), $ntPassword, nt_decrypt( $REG_DATE_TIME, $CRYPTO_PASSWORD ), $RESOURCE_NAME, $RESOURCE_ROLE )

		Dim $result[5] = [ $NT_AUTH_STATUS_PASS, nt_decrypt( $REG_NT_DOMAIN, $CRYPTO_PASSWORD ), nt_decrypt( $REG_NT_USER, $CRYPTO_PASSWORD ), $ntPassword, nt_decrypt( $REG_DATE_TIME, $CRYPTO_PASSWORD )  ]

	Case Else ; $NT_AUTH_STATUS_NONE

		Dim $result[5] = [ $NT_AUTH_STATUS_NONE, "", "", "", ""  ]

	EndSwitch

	Return $result

EndFunc

Func nt_dateFormat( Const $DATE_TIME )

	Local Const $MONTHS[13] = [ "en_US", "Jan.", "Feb.", "Mar.", "Apr.", "May", "Jun.",  "Jul.", "Aug.", "Sept.", "Oct.", "Nov.", "Dec." ]

	Local Const $DAY_DIGITS = Int( StringMid( $DATE_TIME, 9, 2 ) )
	Local Const $MON_DIGITS = Int( StringMid( $DATE_TIME, 6, 2 ) )
	Local Const $YR_STRING = StringMid( $DATE_TIME, 1, 4 )

	Local Const $HOUR_DIGITS = ( Mod( ( Int( StringMid( $DATE_TIME, 14, 2 ) ) - 1 ), 12 ) + 1 )
	Local Const $MIN_STRING = StringMid( $DATE_TIME, 17, 2 )
	Local Const $SEC_STRING = StringMid( $DATE_TIME, 20, 2 )

	Local $append = ""

	If ( Int( StringMid( $DATE_TIME, 14, 2 ) ) < 12 ) Then
		$append = "AM"

	Else
		$append = "PM"
	EndIf

	Local Const $FMT_DATE = $MONTHS[ $MON_DIGITS ] & " " & $DAY_DIGITS & ", " & $YR_STRING
	Local Const $FMT_TIME = $HOUR_DIGITS & ":" & $MIN_STRING & " " & $append

	Return ( $FMT_DATE & " at " & $FMT_TIME )


EndFunc

Func nt_CryptoPassword( Const $REQ_NONCE_ID )

	Local Const $NT_DOMAIN_USER = ( @LogonDomain & "\" & @UserName )
	Local Const $NT_DOMAIN_SID = ( _Security__SidToStringSid( _Security__GetAccountSid( $NT_DOMAIN_USER ) ) )

	Return ( @LogonDomain & $NT_DOMAIN_SID & $REQ_NONCE_ID )

EndFunc

Func nt_encrypt( Const $DATA, Const $PASSWORD )

	Return ( _Crypt_EncryptData( $DATA, $PASSWORD, $CRYPTO_SYMMETRIC_ALGORITHM ) )

EndFunc

Func nt_decrypt( Const $DATA, Const $PASSWORD )

	Return BinaryToString( _Crypt_DecryptData( ( $DATA ), $PASSWORD, $CRYPTO_SYMMETRIC_ALGORITHM ) )

EndFunc


Func nt_hash( Const $DATA, Const $SALT )

	Return StringTrimLeft( ( _Crypt_HashData ( $SALT & $DATA, $CRYPTO_HASH_ALGORITHM ) ), 0 )

EndFunc

Func nt_auth( Const $NT_DOMAIN, Const $NT_USER, Const $NT_PASSWORD, Const $RESOURCE_NAME, Const $RESOURCE_ROLE, Const $SAVE_ONLY = False, Const $READ_ONLY = False )

	Local Const $REG_ROOT_KEY_PATH = nt_RegPath( $RESOURCE_NAME, $RESOURCE_ROLE )
	Local Const $NT_DOMAIN_USER = ( $NT_DOMAIN & "\" & $NT_USER )

	Local Const $REQ_NONCE_ID = GUID_Generate()
	Local Const $CRYPTO_PASSWORD = nt_CryptoPassword( $REQ_NONCE_ID )
	Local Const $CRYPTO_HASH_SALT = $CRYPTO_PASSWORD

	Local $ntPassword = $NT_PASSWORD
	Local $dateTime = ( _Date_Time_GetSystemTime() )
	Local Const $FMT_DATE_TIME = StringReplace( StringReplace( ( _Date_Time_SystemTimeToDateTimeStr( $dateTime, 1 ) ), "/", "-" ), " ", " @ " )

	Local $process = $NT_AUTH_STATUS_NONE

	Local Const $SID_DOMAIN_TYPE = ( _Security__LookupAccountName( $NT_DOMAIN_USER ) )

	If ( Not $SAVE_ONLY ) Then

		If Not IsArray(  $SID_DOMAIN_TYPE ) Then

			$process = $NT_AUTH_STATUS_FAIL

		Else

			$process = RunAs( $NT_USER, $NT_DOMAIN, $ntPassword, 0, $TEST_COMMAND, @UserProfileDir, @SW_HIDE )

			If ( $process <> 0 ) Then

				$process =  $NT_AUTH_STATUS_PASS

			Else

				$process = $NT_AUTH_STATUS_FAIL

			EndIf

		EndIf

	Else

		$process = $NT_AUTH_STATUS_SAVE

	EndIf

	; Populate emptry password:
   If ( $ntPassword == "" ) Then $ntPassword = $CRYPTO_HASH_SALT

	; Common data, for pass/fail...

	Local Const $EXISTING_CREDS = nt_recall( $RESOURCE_NAME, $RESOURCE_ROLE )

	Local Const $REG_NT_DOMAIN = nt_encrypt( $NT_DOMAIN, $CRYPTO_PASSWORD )
	Local Const $REG_NT_USER = nt_encrypt( $NT_USER, $CRYPTO_PASSWORD )
	Local Const $REG_NT_PASSWORD = nt_encrypt( $ntPassword, $CRYPTO_PASSWORD )

	Local Const $REG_AUTH_RESULT = nt_encrypt( $process, $CRYPTO_PASSWORD )
	Local Const $REG_DATE_TIME = nt_encrypt( $FMT_DATE_TIME, $CRYPTO_PASSWORD )

	Local Const $REG_AUTH_NONCE = $REQ_NONCE_ID
	Local Const $REG_AUTH_SIGNATURE = nt_hash( ( "" & $REG_NT_DOMAIN & $REG_NT_USER & $REG_NT_PASSWORD & $REG_DATE_TIME & $REG_AUTH_RESULT ), $CRYPTO_HASH_SALT )



	RegWrite( $REG_ROOT_KEY_PATH, "Domain", "REG_BINARY", $REG_NT_DOMAIN )
	RegWrite( $REG_ROOT_KEY_PATH, "User", "REG_BINARY", $REG_NT_USER )
	RegWrite( $REG_ROOT_KEY_PATH, "Password", "REG_BINARY", $REG_NT_PASSWORD )

	RegWrite( $REG_ROOT_KEY_PATH, "Recorded", "REG_BINARY", $REG_DATE_TIME  )
	RegWrite( $REG_ROOT_KEY_PATH, "Nonce", "REG_SZ", $REG_AUTH_NONCE )


			If ( $SAVE_ONLY And ( $EXISTING_CREDS[ $NT_AUTH_DOMAIN_IDX ] == $NT_DOMAIN ) And _
				( $EXISTING_CREDS[ $NT_AUTH_USER_IDX ] == $NT_USER ) And _
				( $EXISTING_CREDS[ $NT_AUTH_PASSWORD_IDX ] == $NT_PASSWORD ) And _
				( $EXISTING_CREDS[ $NT_AUTH_STATUS_IDX ] == $NT_AUTH_STATUS_PASS ) _
			) Then


				Local Const $MERGED_SIGNATURE = nt_hash( ( "" & _
					$REG_NT_DOMAIN & $REG_NT_USER & $REG_NT_PASSWORD & _
					$REG_DATE_TIME & nt_encrypt( $NT_AUTH_STATUS_PASS, _
					$CRYPTO_PASSWORD ) ), $CRYPTO_HASH_SALT _
				)

				RegWrite( $REG_ROOT_KEY_PATH, "Status", "REG_BINARY", _
					nt_encrypt( $NT_AUTH_STATUS_PASS, $CRYPTO_PASSWORD ) _
				)

				RegWrite( $REG_ROOT_KEY_PATH, "Signature", "REG_BINARY", $MERGED_SIGNATURE )

			Else

				RegWrite( $REG_ROOT_KEY_PATH, "Status", "REG_BINARY", $REG_AUTH_RESULT )
				RegWrite( $REG_ROOT_KEY_PATH, "Signature", "REG_BINARY", $REG_AUTH_SIGNATURE )

			EndIf

	Return $process


EndFunc

Func GUID_Generate()

    Return StringFormat('%04x%04x-%04x-%04x-%04x-%04x%04x%04x', _
            Random(0, 0xffff), Random(0, 0xffff), _
            Random(0, 0xffff), _
            BitOR(Random(0, 0x0fff), 0x4000), _
            BitOR(Random(0, 0x3fff), 0x8000), _
            Random(0, 0xffff), Random(0, 0xffff), Random(0, 0xffff) _
        )

EndFunc