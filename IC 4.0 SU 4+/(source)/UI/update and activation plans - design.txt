Manifest

Licensee ID (derived from contact's e-mail address)

I am {RAND-SESSION-GUID} | retains GUID and IP address

software.vialink.me/inin/marketplace/updates/update.cgi?package=guid&patch=guid&licensee=id&host=id

software.vialink.me/inin/marketplace/activations/activate.cgi?package=guid&patch=guid&licensee=id&host=id&signature=hash
software.vialink.me/inin/marketplace/activations/deactivate.cgi?package=guid&patch=guid&host=id&signature=hash

success: expects a new "license" file to exist for licensing/download
failure: downloadable "license" file will be void of activation information

developer specifies re-activation terms:
days/hardware

distros need to include update settings

Activation Criteria

Hosts:  Allowed / In Use [details]
Hardware Specs: (max)
users/instances (or instances per user)
primary e-mail:
secondary e-mail:

Conditions and messages

/activations/licensee/package/patch/(hosts)

re-activate after (days)
de-activate after (days)